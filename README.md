 ## Description

In this application, you can **read**, **write** and **comment on** posts.

 ## Main features:
 - **Write** & **Read** posts
 - **Comment** on yours and other user's posts
 - **Search** for posts based on:
    - Title
    - Content
    - Author
    - Tags 
 - **Sort** all posts and users based on:

    for posts:
      - Most/Least Liked
      - Most/Least Commented
      - Newest/Oldest 

    for users:
      - A-Z/Z-A Username
      - Registration date

 - **Filter** all posts based on:
    - Post type
    - Tags

## Link to App

https://forum-system-4379a.firebaseapp.com/

## Link to Repository:

 https://gitlab.com/kokiqn/forum-system

## Project setup
Navigate to forum-system folder and npm install in order to install all project dependencies.
In the project directory, you can run:

**npm run dev**

Runs the app in the development mode.
Open the given path to view it in your browser.
The page will reload when you make changes.

## Home page dark theme:

<img src='./src/assets/images/homepage.png'>

## Home page white theme:

<img src='./src/assets/images/homepageWhitetheme.jpg'>

## Login / Register page:

<img src='./src/assets/images/loginpage.png'>
<img src='./src/assets/images/registerpage.png'>

## Users page:

<img src='./src/assets/images/userspage.png'>

## User profile:

<img src='./src/assets/images/userProfile.png'>

## Create post:

<img src='./src/assets/images/createpost.jpg'>

## Searching for posts:

<img src='./src/assets/images/postsearch.jpg'>

## Searching for users:

<img src='./src/assets/images/usersearch.jpg'>
