import { createContext } from "react";

const AppContext = createContext({
  user: null,
  userData: null,
  addToast() {
    // implementation comes from App.jsx
  },
  setContext() {
    // implementation comes from App.jsx
  }
});

export default AppContext;
