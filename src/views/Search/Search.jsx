import {
  Box, Divider, Flex, Heading, Tab, TabList, TabPanel, TabPanels, Tabs,
  useColorModeValue
} from "@chakra-ui/react";

import LeftComponent from "../../components/LeftComponent/LeftComponent";
import RightComponent from "../../components/RightComponent/RightComponent";
import SearchResultsPosts from "../../components/SearchResult/SearchResultsPosts";
import SearchResultsUsers from "../../components/SearchResult/SearchResultsUsers";


export default function Search({ keyword }) {
  return (
    <Flex marginTop={5}>
      <LeftComponent />
      <Box w="50%">
        <Heading size="lg">{`Results for: ${keyword}`}</Heading>
        <Divider marginBottom={4} />
        <Box
          boxShadow="0px 5px 10px rgba(0, 0, 0, 0.25)"
          border="1px solid"
          borderRadius={5}
          borderColor={useColorModeValue("light.secondary", "dark.primary")}
          bg={useColorModeValue("light.secondary", "dark.primary")}
        >
          <Tabs>
            <TabList>
              <Tab>Posts</Tab>
              <Tab>Users</Tab>
            </TabList>
            <TabPanels>
              <TabPanel><SearchResultsPosts keyword={keyword} /></TabPanel>
              <TabPanel><SearchResultsUsers keyword={keyword} /></TabPanel>
            </TabPanels>
          </Tabs>
        </Box>
      </Box>
      <RightComponent />
    </Flex>
  )
}
