import { Box, Flex, useColorModeValue } from "@chakra-ui/react";
import { useContext, useEffect, useState } from "react";
import { POSTS } from "../../common/enums/sort.enum";
import CreatePost from "../../components/CreatePost/CreatePost";
import Filter from "../../components/Filters/Filter";
import Sort from "../../components/Filters/Sort";
import PostContent from "../../components/Post/PostContent";
import AppContext from "../../context/app.context";
import Post from "../../hoc/Post";
import { getAllPosts } from "../../services/posts.services";
import NoContent from "../../hoc/NoContent"

export default function Posts() {
  const { ...appState } = useContext(AppContext)
  const [posts, setPosts] = useState([])
  const [filtered, setFiltered] = useState([])

  useEffect(() => {
    getAllPosts().then(setPosts).catch(() => {
      appState.addToast("error", "Something went wrong!")
    })
  }, [])

  useEffect(() => {
    getAllPosts().then(setPosts).catch(() => {
      appState.addToast("error", "Something went wrong!")
    })
  }, [appState.userData?.posts])

  useEffect(() => {
    setFiltered([...posts])
  }, [posts])

  return (
    <Box
      padding={2}
      boxShadow="0px 5px 10px rgba(0, 0, 0, 0.25)"
      border="1px solid"
      borderRadius={5}
      borderColor={useColorModeValue("light.secondary", "dark.primary")}
      bg={useColorModeValue("light.secondary", "dark.primary")}
    >
      <Flex margin={4} align="center" justify="space-between">
        <Flex gap={2}>
          <Filter unfiltered={posts} setFiltered={setFiltered} />
          <Sort sortType={POSTS} toBeSorted={filtered} setSorted={setFiltered} />
        </Flex>
        {appState.user && !appState.userData?.isBlock && <CreatePost />}
      </Flex>
      <Box>
        {posts.length === 0 ?
          <NoContent>Be the first to post something!</NoContent> :
          filtered.length === 0 ?
            <NoContent>No posts matching this criteria</NoContent> :
            filtered.map(p => <Post key={p.id} id={p.id} author={p.author}
              PostContent={<PostContent {...p} />}
            />)}
      </Box>
    </Box >
  )
}
