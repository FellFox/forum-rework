import { ArrowDownIcon, ArrowUpIcon } from "@chakra-ui/icons";
import {
  Box, Flex, Link, Table, TableContainer, Tbody, Th, Thead,
  Tr, useColorModeValue
} from "@chakra-ui/react";
import { useEffect, useState } from "react";
import { ASC, DESC, USERS } from "../../common/enums/sort.enum";
import LeftComponent from "../../components/LeftComponent/LeftComponent";
import RightComponent from "../../components/RightComponent/RightComponent";
import SingleUser from "../../components/SingleUser/SingleUser";
import { getAllPosts } from "../../services/posts.services";
import { getAllUsers } from "../../services/users.services";

export default function Users() {
  const [users, setUsers] = useState([])
  const [blocked, setBlocked] = useState([])
  const [sort, setSort] = useState({
    criteria: "",
    order: ASC,
  })
  const [allPosts, setAllPosts] = useState()

  useEffect(() => {
    getAllPosts().then(data => setAllPosts(data)).catch(console.error)
  }, [])

  users?.map(user => {
    const userPosts = allPosts?.filter(post => post.author === user.username)

    return user.rating = userPosts.reduce((rating, post) => {
      post.likedBy ? rating += Object.keys(post.likedBy).length : 0
      post.dislikedBy ? rating -= Object.keys(post.dislikedBy).length : 0

      return rating
    }, 0)

  })

  const sortBy = criteria => {
    if (sort.criteria === criteria) {
      setSort({ ...sort, order: sort.order === ASC ? DESC : ASC })
      return [...users.reverse()]
    }

    setSort({ criteria, order: ASC })

    if (criteria === USERS.BY_USERNAME) {
      setUsers(users.sort((a, b) => a.username.localeCompare(b.username)))
    } else if (criteria === USERS.BY_DATE) {
      setUsers(users.sort((a, b) => a.registeredOn - b.registeredOn))
    } else if (criteria === USERS.BY_RATING) {
      setUsers(users.sort((a, b) => a.rating - b.rating))
    } else {
      setUsers(users.sort((a, b) =>
        (a[criteria] ? Object.keys(a[criteria]).length : 0)
        - (b[criteria] ? Object.keys(b[criteria]).length : 0)))
    }
  }

  useEffect(() => {
    getAllUsers().then(setUsers).catch(console.error)
    setSort({ ...sort, criteria: "" })
  }, [blocked])

  return (
    <Flex
      marginTop={5}>
      <LeftComponent />
      <Box
        w="50%"
        boxShadow="0px 3px 6px rgba(0, 0, 0, 0.25)"
        bg={useColorModeValue("light.secondary", "dark.primary")}>
        <TableContainer
          width="100%"
          bg={useColorModeValue("light.secondary", "dark.primary")}
        >
          <Table>
            <Thead>
              <Tr>
                <Th></Th>
                <Th>
                  <Link onClick={() => sortBy(USERS.BY_USERNAME)}>
                    Username
                    {(sort.criteria === USERS.BY_USERNAME && sort.order === ASC) ? <ArrowUpIcon /> :
                      (sort.criteria === USERS.BY_USERNAME && sort.order === DESC) && <ArrowDownIcon />}
                  </Link>
                </Th>
                <Th>
                  <Link onClick={() => sortBy(USERS.BY_RATING)}>
                    Rating
                    {(sort.criteria === USERS.BY_RATING && sort.order === ASC) ? <ArrowUpIcon /> :
                      (sort.criteria === USERS.BY_RATING && sort.order === DESC) && <ArrowDownIcon />}
                  </Link>
                </Th>
                <Th>
                  <Link onClick={() => sortBy(USERS.BY_POSTS)}>
                    Posts
                    {(sort.criteria === USERS.BY_POSTS && sort.order === ASC) ? <ArrowUpIcon /> :
                      (sort.criteria === USERS.BY_POSTS && sort.order === DESC) && <ArrowDownIcon />}
                  </Link>
                </Th>
                <Th>
                  <Link onClick={() => sortBy(USERS.BY_DATE)}>
                    Joined
                    {(sort.criteria === USERS.BY_DATE && sort.order === ASC) ? <ArrowUpIcon /> :
                      (sort.criteria === USERS.BY_DATE && sort.order === DESC) && <ArrowDownIcon />}
                  </Link>
                </Th>
              </Tr>
            </Thead>
            <Tbody>
              {users.map(u => <SingleUser key={u.id} {...u} {...users.rating} setBlocked={setBlocked} />)}
            </Tbody>
          </Table>
        </TableContainer>
      </Box>
      <RightComponent />
    </Flex >
  )
}
