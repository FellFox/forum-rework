import {
  Box, Button, FormControl, FormErrorMessage,
  FormLabel, Heading, Input, Stack, useColorModeValue
} from "@chakra-ui/react";
import { useEffect } from "react";
import { useForm } from "react-hook-form";
import ReactQuill from "react-quill";
import "react-quill/dist/quill.snow.css";
import {
  MAX_POST_CONTENT_LENGTH,
  MAX_POST_TITLE_LENGTH,
  MIN_POST_CONTENT_LENGTH,
  MIN_POST_TITLE_LENGTH
} from "../../common/constants/constants";
import Tags from "../../components/Tags/Tags";

export default function CreateNewPost({ tags, setTags, onSubmitFunction, prevContent }) {
  const {
    handleSubmit,
    register,
    watch,
    setValue,
    formState: {
      errors,
      isSubmitting
    }
  } = useForm()

  const editorContent = watch("content");

  const onEditorStateChange = editorState => {
    setValue("content", editorState);
  };

  useEffect(() => {
    register("content", {
      required: "This is required!",
      minLength: {
        value: MIN_POST_CONTENT_LENGTH,
        message: `Minimum length should be ${MIN_POST_CONTENT_LENGTH}`
      },
      maxLength: {
        value: MAX_POST_CONTENT_LENGTH,
        message: `Maximum length should be ${MAX_POST_CONTENT_LENGTH}`
      },
      validate:
      {
        emptyComment: value => {
          return (value.replace(/<(.|\n)*?>/g, "").trim().length!==0)
            || "Please enter a value!"}
      }
    });
  }, [register]);

  useEffect(() => {
    setValue("title", prevContent?.title || "")
    setValue("content", prevContent?.content || "")
  }, [])

  return (
    <Box
      align={"center"}
      justify={"center"}
      my={4}
    >
      <Stack>
        <Stack align={"center"}>
          <Heading fontSize={"4xl"}
            textAlign={"center"}>
            Create post
          </Heading>
        </Stack>
        <Box
          rounded={"lg"}
          p={8}
          bg={useColorModeValue("light.secondary", "dark.secondary")}
        >
          <Stack spacing={5}>

            <FormControl id="title" isRequired
              isInvalid={
                errors.title
              }>
              <FormLabel htmlFor='title'>Title</FormLabel>
              <Input id='title' {...register("title", {
                required: "This is required!",
                minLength: {
                  value: MIN_POST_TITLE_LENGTH, message:
                    `Minimum length should be ${MIN_POST_TITLE_LENGTH}`
                },
                maxLength: {
                  value: MAX_POST_TITLE_LENGTH, message:
                    `Maximum length should be ${MAX_POST_TITLE_LENGTH}`
                },
              })} />
              <FormErrorMessage> {
                errors.title && errors.title.message
              } </FormErrorMessage>
            </FormControl>
            <FormControl id="content" isRequired
              isInvalid={
                errors.content
              }>
              <FormLabel htmlFor='content'>Content</FormLabel>
              <ReactQuill theme="snow" value={editorContent}
                onChange={onEditorStateChange} id='content' />
              <FormErrorMessage> {
                errors.content && errors.content.message
              } </FormErrorMessage>
            </FormControl>
            {tags && <Tags tags={tags} setTags={setTags} />}
            <Button
              onClick={handleSubmit(onSubmitFunction)}
              isLoading={isSubmitting}
              variant="accent"
              type='submit'
              size="lg">
              Create
            </Button>
          </Stack>
        </Box>
      </Stack>
    </Box>
  )
}
