import { useForm } from "react-hook-form"
import { useContext, useState } from "react";
import {
  FormErrorMessage,
  FormLabel,
  FormControl,
  Input,
  Button,
  Flex,
  useColorModeValue,
  Box,
  Stack,
  Heading,
  Text,
  InputGroup,
  InputRightElement,
} from "@chakra-ui/react"
import {
  MAX_FIRST_NAME_LENGTH,
  MAX_LAST_NAME_LENGTH,
  MAX_PASSWORD_LENGTH,
  MAX_USERNAME_LENGTH,
  MIN_FIRST_NAME_LENGTH,
  MIN_LAST_NAME_LENGTH,
  MIN_PASSWORD_LENGTH,
  MIN_USERNAME_LENGTH
} from "../../common/constants/constants";
import { ViewIcon, ViewOffIcon } from "@chakra-ui/icons";
import { registerUser } from "../../services/auth.services";
import { createUser, getUser } from "../../services/users.services";
import AppContext from "../../context/app.context";
import { useNavigate } from "react-router-dom";

export default function RegistrationForm() {
  const { setContext,addToast, ...appState } = useContext(AppContext)
  const navigate = useNavigate()

  const {
    handleSubmit,
    register,
    setError,
    formState: {
      errors,
      isSubmitting
    }
  } = useForm()

  const [password, setPassword] = useState("");

  const onRegister = async values => {
    try {
      const user = await getUser(values.username);

      if (user !== null) {
        return setError("username", { message: `User with the username ${values.username} already exists!` })
      }

      const credentials = await registerUser(values.email, values.password);
      try {
        await createUser(
          credentials.user.uid,
          values.firstName,
          values.lastName,
          values.username
        )

        setContext({
          ...appState,
          user: {
            email: credentials.user.email,
            uid: credentials.user.uid
          }
        })

        navigate("/")
      } catch (e) {
        addToast("error","Something went wrong!")
        console.error(e)
      }
    } catch (e) {
      setError("email", { type: e.type, message: "This email already exists!" })
    }
  }

  const [showPassword, setShowPassword] = useState(false);
  const [showConfirmedPassword, setShowConfirmedPassword] = useState(false);
  return (
    <Flex minH={"90vh"}
      paddingTop={10}
      justify={"center"}>

      <Stack spacing={8}
        mx={"auto"}
        maxW={"lg"}
        py={12}
        px={6}>
        <Stack align={"center"}>
          <Heading fontSize={"4xl"}
            textAlign={"center"}>
            Sign up
          </Heading>
          <Text fontSize={"lg"}>
            to enjoy all of our cool features ✌️
          </Text>
        </Stack>

        <Box rounded={"lg"}
          bg={
            useColorModeValue("light.secondary", "dark.primary")
          }
          boxShadow="dark-lg"
          p={8}>
          <Stack spacing={2}>
            <Flex gap={2} >
              <FormControl isRequired isInvalid={
                errors.firstName
              }>
                <FormLabel htmlFor='firstName'>First name</FormLabel>
                <Input id='firstName' {...register("firstName", {
                  required: "This is required!",
                  minLength: {
                    value: MIN_FIRST_NAME_LENGTH, message:
                      `Minimum length should be ${MIN_FIRST_NAME_LENGTH}`
                  },
                  maxLength: {
                    value: MAX_FIRST_NAME_LENGTH, message:
                      `Maximum length should be ${MAX_FIRST_NAME_LENGTH}`
                  },
                })} />

                <FormErrorMessage minBlockSize={5}> {
                  errors.firstName ? errors.firstName.message : "Correct value!"
                } </FormErrorMessage>
              </FormControl>


              <FormControl isRequired isInvalid={
                errors.lastName
              }>
                <FormLabel htmlFor='lastName'>Last name</FormLabel>
                <Input id='lastName' {...register("lastName", {
                  required: "This is required!",
                  minLength: {
                    value: MIN_LAST_NAME_LENGTH, message:
                      `Minimum length should be ${MIN_LAST_NAME_LENGTH}`
                  },
                  maxLength: {
                    value: MAX_LAST_NAME_LENGTH, message:
                      `Maximum length should be ${MAX_LAST_NAME_LENGTH}`
                  },
                })} />
                <FormErrorMessage minBlockSize={5}> {
                  errors.lastName ? errors.lastName.message : "Correct value!"
                } </FormErrorMessage>

              </FormControl>

            </Flex>
            <FormControl id="username" isRequired isInvalid={
              errors.username
            }>
              <FormLabel htmlFor='username'>Username</FormLabel>
              <Input id='username' {...register("username", {
                required: "This is required!",
                minLength: {
                  value: MIN_USERNAME_LENGTH, message:
                    `Minimum length should be ${MIN_USERNAME_LENGTH}`
                },
                maxLength: {
                  value: MAX_USERNAME_LENGTH, message:
                    `Maximum length should be ${MAX_USERNAME_LENGTH}`
                },

              })} />
              <FormErrorMessage > {
                errors.username && errors.username.message
              } </FormErrorMessage>
            </FormControl>
            <FormControl id="email" isRequired isInvalid={
              errors.email
            }>
              <FormLabel>Email address</FormLabel>
              <Input id='email' {...register("email", {
                required: "This is required!",
                pattern: {
                  value: /\S+@\S+\.\S+/,
                  message: "Entered value does not match email format!"
                }
              })} />
              <FormErrorMessage > {
                errors.email && errors.email.message
              } </FormErrorMessage>
            </FormControl>
            <FormControl type={showPassword ? "text" : "password"} id="password" isRequired isInvalid={
              errors.password
            }>
              <FormLabel>Password</FormLabel>
              <InputGroup>
                <Input type={showPassword ? "text" : "password"} id='password' {...register("password", {
                  onChange: e => setPassword(e.target.value),
                  minLength: {
                    value: MIN_PASSWORD_LENGTH, message:
                      `Minimum length should be ${MIN_PASSWORD_LENGTH}`
                  },
                  maxLength: {
                    value: MAX_PASSWORD_LENGTH, message:
                      `Maximum length should be ${MAX_PASSWORD_LENGTH}`
                  },
                  required: "This is required!",
                  validate:
                  {
                    containsUpperLetter: value => /^.*[a-z]+.*/.test(value)
                      || "Password must contain at least one lowercase letter!",
                    containsCapitalLetter: value => /^.*[A-Z]+.*/.test(value)
                      || "Password must contain at least one capital letter!",
                    containsNumber: value => /^.*[0-9]+.*/.test(value)
                      || "Password must contain at least one number!",
                    containsSpecialChar: value => /^.*[!@#$%^&*]+.*/.test(value)
                      || "Password must contain at least one special character!",
                  }
                })} />
                <InputRightElement h={"full"}>
                  {showPassword ? <ViewIcon cursor="pointer" onClick={() =>
                    setShowPassword(showPass => !showPass)
                  } /> : <ViewOffIcon cursor="pointer" onClick={() =>
                    setShowPassword(showPass => !showPass)
                  } />}
                </InputRightElement>
              </InputGroup>
              <FormErrorMessage > {
                errors.password && errors.password.message
              } </FormErrorMessage>
            </FormControl>
            <FormControl id="confirmPassword" isRequired isInvalid={
              errors.confirmPassword
            }>
              <FormLabel>Confirm password</FormLabel>
              <InputGroup>
                <Input type={showConfirmedPassword ? "text" : "password"}
                  id='confirmPassword' {...register("confirmPassword", {
                    required: "This is required!",
                    validate:
                    {
                      isMatched: value => password === value
                        || "The passwords do not match!"
                    }
                  })} />
                <InputRightElement h={"full"}>
                  {showConfirmedPassword ? <ViewIcon cursor="pointer" onClick={() =>
                    setShowConfirmedPassword(showPass => !showPass)
                  } /> : <ViewOffIcon cursor="pointer" onClick={() =>
                    setShowConfirmedPassword(showPass => !showPass)
                  } />}
                </InputRightElement>
              </InputGroup>
              <FormErrorMessage > {
                errors.confirmPassword && errors.confirmPassword.message
              } </FormErrorMessage>
            </FormControl>
            <Button mt={4}
              onClick={handleSubmit(onRegister)}
              isLoading={isSubmitting}
              variant="accent"
              type='submit'
              size="lg">
              Sign up

            </Button>
          </Stack>
        </Box>
      </Stack>
    </Flex>
  )
}
