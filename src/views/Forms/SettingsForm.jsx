import { ViewIcon, ViewOffIcon } from "@chakra-ui/icons"
import {
  Avatar,
  Button,
  Flex,
  FormControl,
  FormErrorMessage,
  FormLabel,
  Input,
  InputGroup,
  InputRightElement,
  Stack
} from "@chakra-ui/react"

import { getDownloadURL, ref, uploadBytes } from "firebase/storage"
import { useContext, useState } from "react"
import { useForm } from "react-hook-form"
import { Navigate } from "react-router-dom"
import {
  MAX_FIRST_NAME_LENGTH,
  MAX_LAST_NAME_LENGTH,
  MAX_PASSWORD_LENGTH,
  MIN_FIRST_NAME_LENGTH,
  MIN_LAST_NAME_LENGTH,
  MIN_PASSWORD_LENGTH
} from "../../common/constants/constants"
import { storage } from "../../firebase/config"
import { changePassword } from "../../services/auth.services"
import { updateUserData } from "../../services/users.services"
import AppContext from "../../context/app.context"
export default function SettingsForm({ onClose }) {

  const { addToast, userData, user, setContext } = useContext(AppContext)
  const updatedUserData = { ...userData }
  const [showPassword, setShowPassword] = useState(false)
  const [showConfirmedPassword, setShowConfirmedPassword] = useState(false)
  const [password, setPassword] = useState("")
  const {
    setError,
    handleSubmit,
    register,
    formState: {
      errors,
      isSubmitting
    }
  } = useForm()


  const onSaveChanges = async values => {
    if (values.profilePicture.length !== 0) {

      const file = values.profilePicture.item(0)
      if (!file) {
        return setError("profilePicture", { message: "Add image!" })
      }
      const fileExtension = file.name.split(".").pop()
      if (fileExtension !== "jpg" && fileExtension !== "jpeg") {
        return setError("profilePicture", { message: "Add valid image format!" })
      }

      try {
        const imageRef = ref(storage, `images/${userData?.username}`)

        const result = await uploadBytes(imageRef, file)
        const url = await getDownloadURL(result.ref)

        await updateUserData("profilePicture", url, userData?.username)
        addToast("success", "You have successfully updated your profile picture!")

        updateUserData.profilePicture = url
      } catch (error) {
        addToast("error", "Something went wrong!")
      }
    }

    if (values.newPassword !== "") {
      try {
        const newPassword = values.newPassword
        await changePassword(newPassword)
        addToast("success", "You have successfully updated your password!")
      }
      catch (error) {
        addToast("error", "Something went wrong!")
      }
    }
    if (userData?.firstName !== values.firstName) {
      try {
        await updateUserData("firstName", values.firstName, userData?.username)
        addToast("success", "You have successfully updated your first name!")
        updateUserData.firstName = values.firstName
      }
      catch (error) {
        addToast("error", "Something went wrong!")
      }
    }
    if (userData?.lastName !== values.lastName) {
      try {
        await updateUserData("lastName", values.lastName, userData?.username)
        addToast("success", "You have successfully updated your last name!")
        updatedUserData.lastName = values.lastName
      }
      catch (error) {
        addToast("error", "Something went wrong!")
      }
    }

    setContext(state => ({
      ...state,
      userData:
      {
        ...userData,
        profilePicture: updatedUserData.profilePicture,
        firstName: updatedUserData.firstName,
        lastName: updatedUserData.lastName,
      }

    }))
    onClose()
    Navigate("")
  }

  return (
    <Stack spacing={2} marginBottom={4}>
      <Flex gap={4} align="center">
        <Avatar src={userData?.profilePicture} size="xl" />
        <FormControl isInvalid={errors.profilePicture}>
          <FormLabel htmlFor="avatar">Change avatar</FormLabel>
          <input type="file" id="avatar"  {...register("profilePicture")} />
          <FormErrorMessage minBlockSize={5}>
            {errors?.profilePicture?.message}
          </FormErrorMessage>
        </FormControl>
      </Flex>
      <FormControl isInvalid={errors.firstName}>
        <FormLabel htmlFor="firstName">First name</FormLabel>
        <Input
          id="firstName"
          defaultValue={userData?.firstName}
          {...register("firstName", {
            minLength: {
              value: MIN_FIRST_NAME_LENGTH,
              message: `Minimum length should be ${MIN_FIRST_NAME_LENGTH}`,
            },
            maxLength: {
              value: MAX_FIRST_NAME_LENGTH,
              message: `Maximum length should be ${MAX_FIRST_NAME_LENGTH}`,
            }
          })} />

        <FormErrorMessage minBlockSize={5}>
          {errors?.firstName?.message}
        </FormErrorMessage>
      </FormControl>

      <FormControl isInvalid={errors.lastName}>
        <FormLabel htmlFor="lastName">Last name</FormLabel>
        <Input
          id="lastName"
          defaultValue={userData?.lastName}
          {...register("lastName", {
            minLength: {
              value: MIN_LAST_NAME_LENGTH,
              message: `Minimum length should be ${MIN_LAST_NAME_LENGTH}`,
            },
            maxLength: {
              value: MAX_LAST_NAME_LENGTH,
              message: `Maximum length should be ${MAX_LAST_NAME_LENGTH}`,
            }
          })} />

        <FormErrorMessage minBlockSize={5}>
          {errors?.lastName?.message}
        </FormErrorMessage>
      </FormControl>

      <FormControl isInvalid={errors.email}>
        <FormLabel htmlFor="email">Email</FormLabel>
        <Input
          disabled={true}
          id="email"
          type="email"
          defaultValue={user?.email}
          {...register("email", {
            pattern: {
              value: /\S+@\S+\.\S+/,
              message: "Entered value does not match email format!"
            }
          })} />

        <FormErrorMessage minBlockSize={5}>
          {errors?.email?.message}
        </FormErrorMessage>
      </FormControl>

      <FormControl type={showPassword ? "text" : "password"} id="password" isInvalid={
        errors.password
      }>
        <FormLabel>New Password</FormLabel>
        <InputGroup>
          <Input type={showPassword ? "text" : "password"} id='password' {...register("password", {
            onChange: e => setPassword(e.target.value),
            minLength: {
              value: MIN_PASSWORD_LENGTH, message:
                `Minimum length should be ${MIN_PASSWORD_LENGTH}`
            },
            maxLength: {
              value: MAX_PASSWORD_LENGTH, message:
                `Maximum length should be ${MAX_PASSWORD_LENGTH}`
            },
            validate:
            {
              containsUpperLetter: value => /^.*[a-z]+.*/.test(value)
                || value === "" || "Password must contain at least one lowercase letter!",
              containsCapitalLetter: value => /^.*[A-Z]+.*/.test(value)
                || value === "" || "Password must contain at least one capital letter!",
              containsNumber: value => /^.*[0-9]+.*/.test(value)
                || value === "" || "Password must contain at least one number!",
              containsSpecialChar: value => /^.*[!@#$%^&*]+.*/.test(value)
                || value === "" || "Password must contain at least one special character!",
            }
          })} />
          <InputRightElement h={"full"}>
            {showPassword ? <ViewIcon cursor="pointer" onClick={() =>
              setShowPassword(showPass => !showPass)
            } /> : <ViewOffIcon cursor="pointer" onClick={() =>
              setShowPassword(showPass => !showPass)
            } />}
          </InputRightElement>
        </InputGroup>
        <FormErrorMessage > {
          errors.password && errors.password.message
        } </FormErrorMessage>
      </FormControl>

      <FormControl id="newPassword" isInvalid={
        errors.newPassword
      }>
        <FormLabel>Confirm password</FormLabel>
        <InputGroup>
          <Input type={showConfirmedPassword ? "text" : "password"}
            id='newPassword' {...register("newPassword", {
              minLength: {
                value: MIN_PASSWORD_LENGTH, message:
                  `Minimum length should be ${MIN_PASSWORD_LENGTH}`
              },
              maxLength: {
                value: MAX_PASSWORD_LENGTH, message:
                  `Maximum length should be ${MAX_PASSWORD_LENGTH}`
              },
              validate:
              {
                containsUpperLetter: value => /^.*[a-z]+.*/.test(value)
                  || value === "" || "Password must contain at least one lowercase letter!",
                containsCapitalLetter: value => /^.*[A-Z]+.*/.test(value)
                  || value === "" || "Password must contain at least one capital letter!",
                containsNumber: value => /^.*[0-9]+.*/.test(value)
                  || value === "" || "Password must contain at least one number!",
                containsSpecialChar: value => /^.*[!@#$%^&*]+.*/.test(value)
                  || value === "" || "Password must contain at least one special character!",
                isMatched: value => password === value
                  || "The passwords do not match!"
              }
            })} />
          <InputRightElement h={"full"}>
            {showConfirmedPassword ? <ViewIcon cursor="pointer" onClick={() =>
              setShowConfirmedPassword(showPass => !showPass)
            } /> : <ViewOffIcon cursor="pointer" onClick={() =>
              setShowConfirmedPassword(showPass => !showPass)
            } />}
          </InputRightElement>
        </InputGroup>
        <FormErrorMessage > {
          errors.newPassword && errors.newPassword.message
        } </FormErrorMessage>
      </FormControl>
      <Button
        mt={4}
        onClick={handleSubmit(onSaveChanges)}
        isLoading={isSubmitting}
        variant="accent"
        type="submit"
      >
        Save changes
      </Button>
    </Stack>
  )
}
