import {
  Flex,
  Box,
  FormControl,
  FormLabel,
  Input,
  Link as ChakraLink,
  Stack,
  Button,
  Heading,
  Text,
  useColorModeValue,
  FormErrorMessage,
  InputGroup,
  InputRightElement
} from "@chakra-ui/react";
import { ViewIcon, ViewOffIcon } from "@chakra-ui/icons";
import { useContext } from "react";
import { useState } from "react";
import { Link, useLocation, useNavigate } from "react-router-dom"
import { INVALID_EMAIL, USER_NOT_FOUND, WRONG_PASS } from "../../common/enums/error-enum";
import AppContext from "../../context/app.context";
import { loginUser } from "../../services/auth.services";

export default function LogInForm() {
  const { setContext, ...appState } = useContext(AppContext)
  const [showPassword, setShowPassword] = useState(false);
  const navigate = useNavigate()
  const location = useLocation()

  const [formData, setFormData] = useState({
    email: {
      value: "",
      error: "",
    },
    password: {
      value: "",
      error: "",
    }
  })

  const setError = msg => {
    if (msg.includes(USER_NOT_FOUND)) {
      setFormData({
        ...formData,
        email: {
          ...formData.email,
          error: "User with this email doesn't exist!"
        }
      });
    } else if (msg.includes(INVALID_EMAIL)) {
      setFormData({
        ...formData,
        email: {
          ...formData.email,
          error: "Invalid email!"
        }
      });
    } else if (msg.includes(WRONG_PASS)) {
      setFormData({
        ...formData,
        password: {
          ...formData.password,
          error: "Wrong password!"
        }
      });
    }
  }

  const onLogin = async () => {
    try {
      const credentials = await loginUser(formData.email.value, formData.password.value)

      setContext({
        ...appState,
        user: {
          email: credentials.user.email,
          uid: credentials.user.uid,
        }
      })
      location.state === null ? navigate(-1) : navigate(location.state.from.pathname)
    } catch (e) {
      setError(e.message);
    }
  }

  return (
    <Flex
      minH={"90vh"}
      paddingTop={10}
      justify={"center"}>
      <Stack spacing={8} mx={"auto"} maxW={"lg"} py={12} px={6}>
        <Stack align={"center"}>
          <Heading fontSize={"4xl"}>Sign in to your account</Heading>
          <Text fontSize={"lg"}>
          </Text>
          <Text variant="lighter" fontSize="md">
            Don{"'"}t have an account? Make one
            <Link to="/register">
              <ChakraLink variant="accent"> here</ChakraLink>
            </Link>
          </Text>
        </Stack>
        <Box
          rounded={"lg"}
          bg={useColorModeValue("light.secondary", "dark.primary")}
          boxShadow={"dark-lg"}
          p={8}>
          <Stack spacing={4}>
            <FormControl id="email" isInvalid={formData.email.error}>
              <FormLabel>Email address</FormLabel>
              <Input type="email"
                value={formData.email.value}
                onChange={e => setFormData({
                  ...formData,
                  email: {
                    error: "",
                    value: e.target.value
                  }
                })}
              />
              <FormErrorMessage minBlockSize={5}>
                {formData.email.error ? formData.email.error : "Correct value!"}
              </FormErrorMessage>
            </FormControl>
         
            <FormControl type={showPassword ? "text" : "password"}
              id="password"
              isInvalid={formData.password.error}>
              <FormLabel>Password</FormLabel>
              <InputGroup>
                
                <Input type={showPassword ? "text" : "password"}
                  value={formData.password.value}
                  onChange={e => setFormData({
                    ...formData,
                    password: {
                      error: "",
                      value: e.target.value
                    }
                  })}
                />
                <InputRightElement h={"full"}>
                  {showPassword ? <ViewIcon cursor="pointer" onClick={() =>
                    setShowPassword(showPass => !showPass)
                  } /> : <ViewOffIcon cursor="pointer" onClick={() =>
                    setShowPassword(showPass => !showPass)
                  } />}
                </InputRightElement>
              </InputGroup>
              <FormErrorMessage>
                {formData.password.error ? formData.password.error : "Correct value!"}
              </FormErrorMessage>
            </FormControl>
            <Stack spacing={6}>
              <Button _hover={{
                bg: useColorModeValue("light.accent", "dark.accent"),
                color: useColorModeValue("light.primary", "dark.primary")
              }}
              onClick={onLogin}>
                Sign in
              </Button>
            </Stack>
          </Stack>
        </Box>
      </Stack>
    </Flex>
  );
}
