import { Box, Divider, Flex, Heading } from "@chakra-ui/react";
import { useEffect } from "react";
import { useState } from "react";
import { useParams } from "react-router-dom";
import ProfileCard from "../../components/Profile/ProfileCard";
import ProfileContent from "../../components/Profile/ProfileContent";
import { getUser } from "../../services/users.services";
import AppContext from "../../context/app.context";
import { useContext } from "react";

export default function Profile() {
  const { userData } = useContext(AppContext)
  const [profileData, setProfileData] = useState({});
  const { username } = useParams();

  useEffect(() => {
    getUser(username).then(setProfileData).catch(console.error)
  }, [username,userData])

  return (
    <Box w="50%" marginInline="auto" my={5}>
      <Heading marginTop={5} size="md">{`${username}'s profile page`}</Heading>
      <Divider marginBottom={4} />
      <Flex
        direction={{ base: "column-reverse" }}
        w="100%"
        gap={7}
      >
        <ProfileContent user={username} />
        <ProfileCard {...profileData} isOwner={username === userData?.username} />
      </Flex>
    </Box >
  )
}
