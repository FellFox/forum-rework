import { Box, Flex } from "@chakra-ui/react";
export default function NotFound() {
  return (
    <Flex>
      <Box w="50%">Page not found</Box>
    </Flex>
  )
}