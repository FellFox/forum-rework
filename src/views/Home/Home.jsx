import { Box, Flex } from "@chakra-ui/react";
import LeftComponent from "../../components/LeftComponent/LeftComponent";
import RightComponent from "../../components/RightComponent/RightComponent";
import Posts from "../Posts/Posts";


export default function Home() {
  return (
    <Flex marginTop={5}>
      <LeftComponent />
      <Box w="50%">
        <Posts />
      </Box>
      <RightComponent />
    </Flex>
  )
}
