
import { Box, Flex, useColorModeValue } from "@chakra-ui/react"
import { useContext, useEffect, useState } from "react"
import { useParams } from "react-router-dom"
import { COMMENTS } from "../../common/enums/sort.enum"
import CommentForm from "../../components/Comments/CommentForm"
import Comments from "../../components/Comments/Comments"
import Sort from "../../components/Filters/Sort"
import LeftComponent from "../../components/LeftComponent/LeftComponent"
import NoContent from "../../hoc/NoContent"
import RightComponent from "../../components/RightComponent/RightComponent"
import SinglePostContent from "../../components/SinglePostContent/SinglePostContent"
import AppContext from "../../context/app.context"
import Post from "../../hoc/Post"
import { getCommentById, getCommentForPost } from "../../services/comments.services"
import { getPostById } from "../../services/posts.services"

export default function SinglePost() {
  const { userData } = useContext(AppContext)
  const [post, setPost] = useState()
  const [comments, setComments] = useState([])
  const { id } = useParams()

  useEffect(() => {

    getCommentForPost(id)
      .then(commentsFromDB => {
        const ids = Object.keys(commentsFromDB)

        return Promise.all(ids.map(getCommentById))
          .then(comment => {
            setComments([...comment])
          })
      })
      .catch(console.log)
  }, [id, userData?.comments])

  useEffect(() => {
    getPostById(id).then(setPost).catch(() => {

    })
  }, [userData?.posts])



  return (
    <>
      <Flex my={5}>
        <LeftComponent />
        <Box
          h="max-content"
          maxW="50%"
          minW="50%"
          boxShadow="0px 5px 10px rgba(0, 0, 0, 0.25)"
          borderRadius={5}
          bg={useColorModeValue("light.secondary", "dark.primary")}
        >
          <Post id={id} PostContent={<SinglePostContent {...post} />} />
          <Box
            margin={3}
            borderRadius={5}
            borderColor={useColorModeValue("light.font.primary", "dark.font.secondary")}
            bg={useColorModeValue("light.secondary", "dark.primary")}
          >
            <CommentForm setComments={setComments} postId={id} />
            {comments.length !== 0 ? <>
              <Sort sortType={COMMENTS} toBeSorted={comments} setSorted={setComments} />
              <Comments comments={comments} setComments={setComments} /></> :
              <NoContent>This post has no comments yet!</NoContent>}
          </Box>
        </Box>
        <RightComponent />
      </Flex>
    </>
  )
}
