// import { ref, push, get, set, update, query, equalTo, orderByChild, orderByKey } from "firebase/database"
import { ref, get, set, orderByChild, equalTo, query, update } from "firebase/database"
import { userRole } from "../common/enums/user-role.enum"
import { db } from "../firebase/config"

/**
 * @description - get user object by it's username
 * @param {string} username - username of the user 
 * @returns Promise object represented content of the user object:
 * null if user doesn't exists or object with information about the user otherwise
 */
export const getUser = async username => {
  const snapshot = await get(ref(db, `users/${username}`))

  return snapshot.val() ? { ...snapshot.val() } : null;
}

/**
 * Gets all users from the database
 * @returns {Object | null} the users collection
 */
export const getAllUsers = async () => {
  const snapshot = await get(ref(db, "users"))

  if (!snapshot.exists()) {
    return []
  }

  return Object
    .values(snapshot.val())
}

/**
 * @description - get user object by it's uid
 * @param {string} uid - id of the user 
 * @returns Promise object represented content of the user object:
 * null if user doesn't exists or object with information about the user otherwise
 */
export const getUserById = async uid => {
  const snapshot = await get(query(ref(db, "users"), orderByChild("uid"), equalTo(uid)));
  const value = snapshot.val();

  return value ? {
    ...Object.values(value)[0],
    comments: Object.values(value)[0]?.comments ?
      Object.keys(Object.values(value)[0].comments) : [],
    posts: Object.values(value)[0]?.posts ?
      Object.keys(Object.values(value)[0].posts) : [],
    likedPosts: Object.values(value)[0]?.likedPosts ?
      Object.keys(Object.values(value)[0].likedPosts) : [],
    dislikedPosts: Object.values(value)[0]?.dislikedPosts ?
      Object.keys(Object.values(value)[0].dislikedPosts) : [],
    likedComments: Object.values(value)[0]?.likedComments ?
      Object.keys(Object.values(value)[0].likedComments) : [],
    dislikedComments: Object.values(value)[0]?.dislikedComments ?
      Object.keys(Object.values(value)[0].dislikedComments) : [],
  } : value;
}

/**
 * @description - create user in database if it doesn't exist or throw error otherwise 
 * @param {string} uid 
 * @param {string} firstName 
 * @param {string} lastName 
 * @param {string} username 
 * @param {number} role 
 */
export const createUser = async (uid, firstName, lastName, username, role = userRole.BASIC) => {
  const user = await getUser(username)

  if (user !== null) throw new Error(`User with username ${username} already exists!`)

  const userData = {
    uid, firstName, lastName, username, role,
    registeredOn: Date.now(), profilePicture: null, likedPosts: null, dislikedPosts: null
  }

  await set(ref(db, `users/${username}`), userData)

  return { ...userData, posts:[], commentedPosts: [], likedPosts: [], dislikedPosts: [] }
}

export const updateUserData = async (propName, propData, username) => {
  return update(ref(db), {
    [`users/${username}/${propName}`]: propData,
  })
}
