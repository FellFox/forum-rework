import { equalTo, get, orderByChild, push, query, ref, update } from "firebase/database"
import { db } from "../firebase/config"


export const getAllComments = async () => {
  const snapshot = await get(ref(db, "comments"))

  if (!snapshot.exists()) {
    return []
  }

  return Object
    .keys(snapshot.val())
    .map(key => ({ ...snapshot.val()[key], id: key }))
}
/**
 * @description Add comment in database
 * @param {string} author - comment author
 * @param {string} postId - id of the post we are commenting on
 * @param {string} content - comment content
 * @returns {Promise} Promise object of void
 */
export const addComment = async (author, postId, content) => {
  const comment = { author, postId, content, commentedOn: Date.now() }
  const result = await push(ref(db, "comments"), comment).catch(error => { throw new Error(error) })

  await update(ref(db), {
    [`users/${author}/comments/${result.key}`]: true,
    [`posts/${postId}/comments/${result.key}`]: true,
  }).catch(error => { throw new Error(error) })
  return [result.key,{ ...comment }]
}
/**
 * @description Delete comment for post
 * @param {string} postId - id of the post for which we are deleting a comment
 * @param {string} commentId - id of the comment that we delete 
 * @returns {Promise} Promise object of void
 */
export const deleteCommentForPost = async (postId, commentId) => {
  return update(ref(db), {
    [`comments/${commentId}/postId/${postId}`]: null,
  })
}

/**
 * @description Gets the set of comments for given post by its id
 * @param {string} postId - id of the post
 * @returns {Promise} Promise object represents comments for given post
 */
export const getCommentForPost = async id => {
  const snapshot = await get(query(ref(db, "comments"), orderByChild("postId"), equalTo(id))).
    catch(error => { throw new Error(error) })

  if (!snapshot.exists()) return [];

  return {
    ...snapshot.val()
  }
}

/**
 * @description Gets the set of comments for given author username
 * @param {string} author - author of the comment
 * @returns {Promise} Promise object - array of objects represents comments info 
 */
export const getCommentsByAuthor = async author => {
  const snapshot = await get(query(ref(db, "comments"), orderByChild("author"), equalTo(author)))
  if (!snapshot.exists()) return []
  const value = snapshot.val()

  return Object
    .keys(value)
    .map(key => ({ ...value[key], id: key }))
}

/**
 * @description Update the comment by its id
 * @param {string} commentId - id of the comment
 * @param {string} newContent - new content of the comment
 */
export const editCommentInDatabase = async (commentId, newContent) => {
  return update(ref(db), {
    [`comments/${commentId}/content`]: newContent,
    [`comments/${commentId}/commentedOn`]: Date.now()
  })
}
/**
 * @description Delete the comment by its id
 * @param {string} commentId - id of the comment
 * @param {string} postId - id of the post for which it is a comment
 * @param {string} username - the author of the comment
 */
export const deleteCommentfromDatabase = async (commentId, postId, username) => {
  return update(ref(db), {
    [`comments/${commentId}`]: null,
    [`posts/${postId}/comments/${commentId}`]: null,
    [`users/${username}/comments/${commentId}`]: null
  })
}

/**
 * @description Gets the comment by its id
 * @param {string} commentId - id of the comment
 * @returns {Promise} Promise object represents all comments properties
 */
export const getCommentById = async id => {
  const snapshot = await get(ref(db, `comments/${id}`))

  if (!snapshot.exists()) throw new Error(`Comment with id ${id} does not exist!`)
  const value = snapshot.val()

  const result = {
    ...value,
    id,
    likedBy: value?.likedBy ? Object.keys(value.likedBy) : [],
    dislikedBy: value?.dislikedBy ? Object.keys(value.dislikedBy) : [],
  }

  return result
}

export const toggleCommentsLikes = async (commentId, author, like = true) => {
  return update(ref(db), {
    [`users/${author}/likedComments/${commentId}`]: like || null,
    [`comments/${commentId}/likedBy/${author}`]: like || null,
  })
}

export const toggleCommentsDisLikes = async (commentId, author, dislike = true) => {
  return update(ref(db), {
    [`users/${author}/dislikedComments/${commentId}`]: dislike || null,
    [`comments/${commentId}/dislikedBy/${author}`]: dislike || null,
  })
}
