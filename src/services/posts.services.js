import {
  equalTo, get, orderByChild, push, query, ref, update
} from "firebase/database"
import { db } from "../firebase/config"

export const addPost = async (title, content, username) => {
  const post = {
    title,
    content,
    author: username,
    postedOn: Date.now(),
  }
  const { key } = await push(ref(db, "posts"), post)
  // users/pesho/posts/id : true
  update(ref(db), {
    [`users/${username}/posts/${key}`]: true,
  })

  return key
}

export const getAllPosts = async () => {
  const snapshot = await get(ref(db, "posts"))

  if (!snapshot.exists()) {
    return []
  }
  const value = snapshot.val();

  return value ? Object
    .keys(value)
    .map(key => ({
      ...value[key],
      id: key,
      tags: value[key]?.tags ? Object.keys(value[key].tags) : []
    })) : []
}

export const getPostsByAuthor = async author => {
  const snapshot = await get(query(ref(db, "posts"), orderByChild("author"), equalTo(author)));
  const value = snapshot.val();

  return value ? Object
    .keys(value)
    .map(key => ({
      ...value[key],
      id: key,
      tags: value[key]?.tags ? Object.keys(value[key].tags) : []
    })) : []
}

export const getPostById = async id => {
  const snapshot = await get(ref(db, `posts/${id}`))

  if (!snapshot.exists()) throw new Error("Post doesn't exist!")
  const value = snapshot.val()

  const result = {
    ...value,
    id,
    comments: value?.comments ? Object.keys(value.comments) : [],
    likedBy: value?.likedBy ? Object.keys(value.likedBy) : [],
    dislikedBy: value?.dislikedBy ? Object.keys(value.dislikedBy) : [],
    tags: value?.tags ? Object.keys(value.tags) : []
  }

  return result
}

export const getPostByTitle = async title => {
  const snapshot = await get(ref(db, "posts"))

  if (!snapshot.exists()) throw new Error("Post with this title doesn't exists!")

  return Object
    .keys(snapshot.val())
    .map(key => ({ ...snapshot.val()[key], id: key }))
    .filter(p => p.title.toLowerCase().includes(title.toLowerCase()))
}
export const editPostInDatabase = async (postId, prop, newValue) => {
  return update(ref(db), {
    [`posts/${postId}/${prop}`]: newValue,
  })
}

export const deletePostfromDatabase = async (postId, username) => {
  const snapshot = await get(ref(db, `posts/${postId}`));
  const { tags } = snapshot.val()

  if (tags) {
    Object.keys(tags).map(tag => update(ref(db), {
      [`tags/${tag}/posts/${postId}`]: null,
    }))
  }

  return update(ref(db), {
    [`posts/${postId}`]: null,
    [`users/${username}/posts/${postId}`]: null
  })
}

export const togglePostLikes = async (postId, author, like = true) => {
  return update(ref(db), {
    [`users/${author}/likedPosts/${postId}`]: like || null,
    [`posts/${postId}/likedBy/${author}`]: like || null,
  })
}

export const togglePostDisLikes = async (postId, author, dislike = true) => {
  return update(ref(db), {
    [`users/${author}/dislikedPosts/${postId}`]: dislike || null,
    [`posts/${postId}/dislikedBy/${author}`]: dislike || null,
  })
}
