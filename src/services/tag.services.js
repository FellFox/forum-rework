import { get, ref, update } from "firebase/database"
import { db } from "../firebase/config"

export const addTagToPost = async (tag, postId) => {
  return update(ref(db), {
    [`posts/${postId}/tags/${tag}`]: true,
    [`tags/${tag}/posts/${postId}`]: true,
  })
}

/**
 * Gets all tags and their posts from the database
 * @return {[]} 
 */
export const getAllTags = async () => {
  const snapshot = await get(ref(db, "tags"));

  const value = snapshot.val();
  if (!value) return [];

  return Object.keys(value).map(tag => ({
    tag: tag,
    posts: (Object.keys(value[tag]?.posts)),
  }))
}
