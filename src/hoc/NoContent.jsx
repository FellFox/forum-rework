import { Box, Heading } from "@chakra-ui/react";

export default function NoContent({ children }) {
  return (
    <Box textAlign="center" paddingBlock={5}>
      <Heading margin={10} size="md" variant="lighter">{children}</Heading>
    </Box>
  )
}
