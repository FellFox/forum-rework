import { ChatIcon } from "@chakra-ui/icons";
import { Box, Button, Divider, Flex, Link, Text, useColorModeValue } from "@chakra-ui/react";
import { useContext, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { userRole } from "../common/enums/user-role.enum";
import PostAction from "../components/Post/PostAction";
import Rating from "../components/Rating/Rating";
import AppContext from "../context/app.context";
import { getPostById, togglePostDisLikes, togglePostLikes } from "../services/posts.services";

export default function Post({ id, PostContent }) {
  const { setContext, userData,addToast } = useContext(AppContext)
  const [post, setPost] = useState({})
  const navigate = useNavigate()
 
  useEffect(() => {
     
    getPostById(id).then(setPost).catch(() => {
      addToast("error", "Something went wrong!")
    })
  }, [userData?.likedPosts, userData?.dislikedPosts, id,userData?.comments])

  const upVote = () => {
    if (!userData.likedPosts.includes(id)) {
      setContext(state => ({
        ...state,
        userData: {
          ...userData,
          likedPosts: [...userData.likedPosts, id],
          dislikedPosts: userData.dislikedPosts.filter(el => el !== id),
        }
      }))

      togglePostLikes(id, userData.username)
      togglePostDisLikes(id, userData.username, false)
    } else {
      setContext(state => ({
        ...state,
        userData: {
          ...userData,
          likedPosts: userData.likedPosts.filter(el => el !== id)
        }
      }))

      togglePostLikes(id, userData.username, false)
    }
  }

  const downVote = () => {
    if (!userData.dislikedPosts.includes(id)) {
      setContext(state => ({
        ...state,
        userData: {
          ...userData,
          dislikedPosts: [...userData.dislikedPosts, id],
          likedPosts: userData.likedPosts.filter(el => el !== id)
        }
      }))

      togglePostDisLikes(id, userData.username)
      togglePostLikes(id, userData.username, false)
    } else {
      setContext(state => ({
        ...state,
        userData: {
          ...userData,
          dislikedPosts: userData.dislikedPosts.filter(el => el !== id),
        }
      }))

      togglePostDisLikes(id, userData.username, false)
    }
  }

  return (
    <Box
      margin={3}
      minH="150px"
      border="1px solid"
      borderRadius={5}
      borderColor={useColorModeValue("light.font.primary", "dark.font.secondary")}
      bg={useColorModeValue("light.primary", "dark.primary")}
    >
      <Flex>
        <Rating upVote={upVote} downVote={downVote}
          rating={(post?.likedBy?.length - post?.dislikedBy?.length)} />
        {PostContent}
      </Flex>
      <Divider />
      <Flex align="center" justify="space-between">
        <Flex align="center" onClick={() => navigate(`/posts/${id}`)}>
          <Button variant="ghost">
            <ChatIcon />
          </Button>
          <Link><Text fontWeight={500} fontSize="sm">
            {post.comments && Object.keys(post.comments).length || 0} comments</Text></Link>
        </Flex>
        {(post?.author === userData?.username || userData?.role===userRole.ADMIN) ?
          <PostAction postId={id} /> :
          <></>}
      </Flex>
    </Box>
  )
}
