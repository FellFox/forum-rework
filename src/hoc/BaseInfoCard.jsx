import {
  Divider,
  Heading,
  Box,
  useColorModeValue,
} from "@chakra-ui/react";

export default function BaseInfoCard({ title, content, divider = true }) {
  return (
    <Box maxW={"320px"}
      w={"full"}
      bg={useColorModeValue("light.secondary", "dark.primary")}
      borderColor={useColorModeValue("light.font.secondary", "dark.font.secondary")}
      boxShadow="0px 5px 10px rgba(0, 0, 0, 0.25)"
      rounded={5}
      p={6}
      textAlign={"center"}>
      <Heading fontSize={"2xl"} >
        {title}
      </Heading>
      {divider && <Divider marginBottom={3} />}
      <Box align={"center"}
        justify={"center"}>
        {content}
      </Box>
    </Box>
  );
}
