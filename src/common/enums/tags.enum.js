export const POST_TYPE = {
  DISCUSSION: "Discussion",
  REVIEW: "Review",
  RECOMMENDATION: "Recommendation",
}

export const GENRES = {
  ADVENTURE: "Adventure",
  CLASSICS: "Classics",
  DYSTOPIAN: "Dystopian",
  FANTASY: "Fantasy",
  HISTORICAL: "Historical",
  MYSTERY: "Mystery",
  ROMANCE: "Romance",
  SCI_FI: "Science Fiction",
  THRILLER: "Thriller",
}
