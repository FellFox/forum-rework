export const USERS = {
  BY_USERNAME: "username",
  BY_RATING: "rating",
  BY_POSTS: "posts",
  BY_DATE: "registeredOn",
}

export const POSTS = {
  BY_DATE: "postedOn",
  BY_COMMENTS: "comments",
  BY_LIKES: "likedBy",
}

export const COMMENTS = {
  BY_DATE: "commentedOn",
  BY_LIKES: "likedBy",
}

export const ASC = 1;
export const DESC = 2;
