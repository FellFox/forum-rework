import { mode } from "@chakra-ui/theme-tools";

const styles = {
  global: props => ({
    body: {
      bg: mode("light.primary", "dark.secondary")(props),
      fontWeight: 500,
    },
    option: {
      color: mode("light.font.secondary", "dark.font.primary")(props),
    },
    label: {
      color: mode("light.font.secondary", "dark.font.primary")(props),
    }
  })
};

export default styles;
