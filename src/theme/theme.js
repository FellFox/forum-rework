import { extendTheme } from "@chakra-ui/react";
import "@fontsource/open-sans/300.css";
import "@fontsource/open-sans/400.css";
import "@fontsource/open-sans/500.css";
import "@fontsource/open-sans/600.css";
import "@fontsource/open-sans/700.css";
import colors from "./colors.js";
import { components } from "./components/components.js";
import styles from "./styles.js";

const theme = extendTheme({
  styles,
  colors,
  fonts: {
    body: "Open Sans, sans-serif",
  },
  components,
});

export default theme;
