import { tabsAnatomy } from "@chakra-ui/anatomy"
import { createMultiStyleConfigHelpers } from "@chakra-ui/react"
import { mode } from "@chakra-ui/theme-tools"

const { definePartsStyle, defineMultiStyleConfig } =
  createMultiStyleConfigHelpers(tabsAnatomy.keys)

const baseStyle = props => definePartsStyle({
  root: {
    color: mode("light.font.primary", "dark.font.secondary")(props),
  },
  tab: {
    textTransform: "uppercase",
    fontFamily: "heading",
    fontWeight: 600,
  },
  tablist: {
    borderColor: mode("light.font.primary", "dark.font.secondary")(props),
  },
  tabpanel: {
    padding: 0
  }
})

const variantLine = definePartsStyle(props => {
  return {
    tablist: {
      borderBottom: "0",
      boxShadow: "0px 2px 1px rgba(0,0,0, 0.2)",
    },
    tab: {
      _selected: {
        borderColor: mode("light.font.secondary", "dark.font.primary")(props),
        color: mode("light.font.secondary", "dark.font.primary")(props)
      },
      _active: {
        bg: mode("light.primary", "dark.secondary")(props)
      }
    }
  }
})

export const Tabs = defineMultiStyleConfig({
  baseStyle,
  variants: {
    line: variantLine
  }
})
