import { tableAnatomy as parts } from "@chakra-ui/anatomy"
import { createMultiStyleConfigHelpers } from "@chakra-ui/styled-system"
import { mode } from "@chakra-ui/theme-tools"

const { defineMultiStyleConfig, definePartsStyle } =
  createMultiStyleConfigHelpers(parts.keys)

const baseStyle = props => definePartsStyle({
  table: {
    color: mode("light.font.primary", "dark.font.primary")(props),
    borderCollapse: "collapse",
    width: "full",
  },
  th: {
    fontFamily: "Open Sans",
    fontWeight: "bold",
    textTransform: "uppercase",
    letterSpacing: "wider",
    textAlign: "start",
  },
  td: {
    textAlign: "start",
    fontWeight: 500,
  },
  caption: {
    mt: 4,
    fontFamily: "heading",
    textAlign: "center",
    fontWeight: "medium",
  },
})

const variantSimple = definePartsStyle(props => {
  return {
    th: {
      color: mode("light.font.primary", "dark.font.primary")(props),
      borderBottom: "1px",
      borderColor: mode("light.font.primary", "dark.font.secondary")(props),
    },
    td: {
      border: "none",
    },
    tr: {
      "&:nth-child(even)": {
        bg: mode("light.secondary", "dark.primary")(props)
      }
    },
    tfoot: {
      tr: {
        "&:last-of-type": {
          th: { borderBottomWidth: 0 },
        },
      },
    },
  }
})

export const Table = defineMultiStyleConfig({
  baseStyle,
  variants: { simple: variantSimple }
})
