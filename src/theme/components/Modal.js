import colors from "../colors.js";
import { mode } from "@chakra-ui/theme-tools";
import { modalAnatomy as parts } from "@chakra-ui/anatomy"
import { createMultiStyleConfigHelpers } from "@chakra-ui/styled-system"

const { definePartsStyle, defineMultiStyleConfig } =
  createMultiStyleConfigHelpers(parts.keys)

const baseStyle = props => definePartsStyle({
  overlay: {
    bg: "blackAlpha.300",
  },
  header: {
    color: mode(colors.light.font.secondary, colors.dark.font.primary)(props),
  },
  dialog: {
    bg: mode(colors.light.primary, colors.dark.primary)(props),
  },
  closeButton: {
    _hover: {
      bg: mode(colors.light.font.primary, colors.dark.font.secondary)(props),
      color: "#f4f4f4",
    },
  }
})

export const Modal = defineMultiStyleConfig({
  baseStyle,
})
