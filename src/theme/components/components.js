import colors from "../colors.js";
import { mode } from "@chakra-ui/theme-tools";
import { Modal } from "./Modal.js";
import { Table } from "./Table.js";
import { Tabs } from "./Tabs.js";

const Avatar = {
  baseStyle: {
    container: {
      _hover: {
        cursor: "pointer",
      }
    }
  }
}

const Button = {
  baseStyle: props => ({
    color: mode(colors.light.font.secondary, colors.dark.font.primary)(props),
    fontWeight: 700,
    borderRadius: 10,
    boxShadow: "0px 2px 4px rgba(0, 0, 0, 0.2)",
  }),
  variants: {
    solid: props => ({
      bg: mode(colors.light.primary, colors.dark.secondary)(props),
      _hover: {
        bg: mode(colors.light.font.primary, colors.dark.font.secondary)(props),
        color: "#f4f4f4",
      },
      _active: {
        bg: mode(colors.light.font.primary, colors.dark.font.secondary)(props),
      }
    }),
    outlined: props => ({
      bg: mode(colors.light.primary, colors.dark.primary)(props),
      boxShadow: "none",
      border: "1px solid",
      borderColor: mode(colors.light.font.primary, colors.dark.font.secondary)(props),
      _hover: {
        bg: mode(colors.light.font.primary, colors.dark.font.secondary)(props),
        color: "#f4f4f4",
      },
      _active: {
        bg: mode(colors.light.font.primary, colors.dark.font.secondary)(props),
        color: "#f4f4f4",
      }
    }),
    ghost: props => ({
      bg: mode(colors.light.primary, colors.dark.primary)(props),
      color: mode(colors.light.font.secondary, colors.dark.font.primary)(props),
      boxShadow: "none",
      padding: 0,
      _hover: {
        bg: mode(colors.light.primary, colors.dark.primary)(props)
      },
      _active: {
        bg: mode(colors.light.primary, colors.dark.primary)(props)
      }
    }),
    accent: props => ({
      bg: mode(colors.light.accent, colors.dark.accent)(props),
      color: mode(colors.light.primary, colors.dark.primary)(props),
      _hover: {
        transform: "scale(1.05)"
      }
    }),
  },
};

const Checkbox = {
  baseStyle: props => ({
    control: {
      borderColor: mode("light.font.primary", "dark.font.secondary")(props),
      _checked: {
        bg: mode("light.accent", "dark.accent")(props),
        border: "none",
        _hover: {
          bg: mode("light.accent", "dark.accent")(props),
          border: "none"
        }
      }
    }
  })
}

const Container = {
  baseStyle: props => ({
    bg: mode(colors.light.primary, colors.dark.primary)(props),
  }),
};

const Heading = {
  baseStyle: props => ({
    color: mode("light.font.secondary", "dark.font.primary")(props),
    fontFamily: "Open Sans",
  }),
  variants: {
    lighter: props => ({
      color: mode("light.font.primary", "dark.font.secondary")(props),
    }),
    accent: props => ({
      color: mode("light.accent", "dark.accent")(props),
    }),
  }
}

const Divider = {
  baseStyle: {
    borderColor: "light.font.primary",
    _dark: {
      borderColor: "dark.font.secondary"
    }
  }
};

const Input = {
  baseStyle: props => ({
    field: {
      color: mode(colors.light.font.secondary, colors.dark.font.primary)(props),
      _placeholder: {
        color: mode(colors.light.font.primary, colors.dark.font.secondary)(props),
      }
    }
  }),
  variants: {
    outline: props => ({
      field: {
        border: "1px solid",
        borderColor: mode(colors.light.font.primary, colors.dark.font.secondary)(props),
        _hover: {
          border: "1px solid",
          borderColor: mode(colors.light.font.primary, colors.dark.font.secondary)(props),
          boxShadow: "none",
        },
        _focus: {
          border: "1px solid",
          borderColor: mode(colors.light.font.secondary, colors.dark.font.secondary)(props),
          boxShadow: "none",
        }
      }
    }),
    filled: props => ({
      field: {
        bg: mode(colors.light.primary, colors.dark.secondary)(props),
        boxShadow: "0px 4px 6px rgba(0, 0, 0, 0.25)",
        _hover: {
          bg: mode(colors.light.primary, colors.dark.secondary)(props),
        },
        _focus: {
          border: "1px solid",
          bg: mode(colors.light.primary, colors.dark.secondary)(props),
          borderColor: mode(colors.light.font.secondary, colors.dark.font.secondary)(props),
        }
      }
    }),
  }
};


const Textarea = {
  baseStyle: props => ({
    fontFamily: "Open Sans",
    color: mode("light.font.secondary", "dark.font.primary")(props),
    fontWeight: 400,
    border: "1px solid",
  }),
  variants: {
    outline: props => ({
      border: "1px solid",
      borderColor: mode(colors.light.font.primary, colors.dark.font.secondary)(props),
      _hover: {
        border: "1px solid",
        borderColor: mode(colors.light.font.primary, colors.dark.font.secondary)(props),
        boxShadow: "none",
      },
      _placeholder: {
        color: mode("light.font.primary", "dark.font.secondary")(props),
      },
      _focus: {
        border: "1px solid",
        borderColor: mode(colors.light.font.secondary, colors.dark.font.secondary)(props),
        boxShadow: "none",
      }

    }),
    filled: props => ({
      bg: mode(colors.light.primary, colors.dark.secondary)(props),
      boxShadow: "0px 4px 6px rgba(0, 0, 0, 0.25)",
      _hover: {
        bg: mode(colors.light.primary, colors.dark.secondary)(props),
      },
      _focus: {
        border: "1px solid",
        bg: mode(colors.light.primary, colors.dark.secondary)(props),
        borderColor: mode(colors.light.font.secondary, colors.dark.font.secondary)(props),
      }
    }),
  }
};

const Link = {
  variants: {
    accent: props => ({
      color: mode("light.accent", "dark.accent")(props)
    })
  }
};

const Menu = {
  baseStyle: props => ({
    list: {
      bg: mode("light.primary", "dark.primary")(props),
      color: mode("light.font.secondary", "dark.font.primary")(props),
      // borderColor: mode("light.font.primary", "dark.font.secondary")(props),
      border: "none",
      boxShadow: "xl",
    },
    item: {
      bg: mode("light.primary", "dark.primary")(props),
      _hover: {
        bg: mode("light.secondary", "dark.secondary")(props),
      },
      _focus: {
        bg: mode("light.secondary", "dark.secondary")(props),
      }
    },
    divider: {
      borderColor: mode("light.font.primary", "dark.font.secondary")(props)
    }
  })
};

const Select = {
  variants: {
    filled: props => ({
      field: {
        fontWeight: 600,
        borderRadius: 20,
        color: mode("light.primary", "dark.primary")(props),
        bg: mode("light.accent", "dark.accent")(props),
        _hover: {
          bg: mode("light.accent", "dark.accent")(props),
        },
        _focus: {
          bg: mode("light.accent", "dark.accent")(props),
          borderColor: mode("light.accent", "dark.accent")(props),
        }
      },
      icon: {
        color: mode("light.primary", "dark.primary")(props),
      }
    }),
    outline: props => ({
      field: {
        fontWeight: 600,
        borderRadius: 20,
        border: "1px solid",
        borderColor: mode("light.font.secondary", "dark.font.secondary")(props),
        color: mode("light.font.secondary", "dark.font.primary")(props),
        bg: mode("light.primary", "dark.primary")(props),
        _hover: {
          borderColor: mode("light.font.secondary", "dark.font.secondary")(props),
        },
        _focus: {
          borderColor: mode("light.font.secondary", "dark.font.secondary")(props),
          boxShadow: "none",
        }
      }
    })
  }
}

const Stat = {
  baseStyle: props => ({
    label: {
      color: mode("light.font.secondary", "dark.font.primary")(props)
    },
    number: {
      color: mode("light.font.secondary", "dark.font.primary")(props)
    }
  })
};

const Tag = {
  variants: {
    solid: props => ({
      container: {
        color: mode("light.primary", "dark.primary")(props),
        bg: mode("light.accent", "dark.accent")(props),
        borderRadius: 20,
      },
    })
  }
};

const Text = {
  baseStyle: props => ({
    color: mode("light.font.secondary", "dark.font.primary")(props),
    fontFamily: "Open Sans",
    fontWeight: 400,
  }),
  variants: {
    lighter: props => ({
      color: mode("light.font.primary", "dark.font.secondary")(props),
    }),
    accent: props => ({
      color: mode("light.accent", "dark.accent")(props),
    }),
  }
};

export const components = {
  Avatar,
  Modal,
  Button,
  Container,
  Divider,
  Heading,
  Input,
  Link,
  Menu,
  Checkbox,
  Select,
  Stat,
  Table,
  Tabs,
  Tag,
  Text,
  Textarea
};
