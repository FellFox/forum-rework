const light = {
  "primary": "#F4F1EA",
  "secondary": "#E9E1D8",
  "accent": "#A15E49",
  font: {
    "primary": "#806D5F",
    "secondary": "#462F04",
  }
}

const dark = {
  "primary": "#2C2C2C",
  "secondary": "#3b3b3b",
  "accent": "#e17d47",
  font: {
    "primary": "#F4F4F4",
    "secondary": "#878787",
  }
}

const colors = {
  light,
  dark,
};

export default colors;
