import { useColorMode, useToast } from "@chakra-ui/react"
import { useEffect, useState } from "react"
import { useAuthState } from "react-firebase-hooks/auth"
import { Route, Routes, useSearchParams } from "react-router-dom"
import Authenticated from "./hoc/Authenticated"
import Header from "./components/Header/Header"
import AppContext from "./context/app.context"
import { auth } from "./firebase/config"
import { getUserById } from "./services/users.services"
import LogInForm from "./views/Forms/LogInForm"
import RegistrationForm from "./views/Forms/RegistrationForm"
import Home from "./views/Home/Home"
import NotFound from "./views/NotFound/NotFound"
import Profile from "./views/Profile/Profile"
import Search from "./views/Search/Search"
import SinglePost from "./views/SinglePost/SinglePost"
import Users from "./views/Users/Users"


function App() {
  const toast = useToast()
  const [user, loading] = useAuthState(auth)

  const [search, setSearch] = useSearchParams()
  const [keyword, setKeyword] = useState("")

  const setSearchParams = value => {
    setKeyword(value)
    setSearch({ query: value })
  }

  const [appState, setAppState] = useState({
    user: user ? { email: user.email, uid: user.uid } : null,
    userData: null
  })

  /**
   * 
   * @param {'success' | 'error'} type 
   * @param {string} message 
   * @param {string} title 
   */
  const addToast = (type, message) => {
    return toast({
      description: message,
      status: type,
      duration: 3000,
      isClosable: true,
    })
  }
  const { colorMode, toggleColorMode } = useColorMode()

  useEffect(() => {
    setAppState({
      ...appState,
      user: user ? { email: user.email, uid: user.uid } : null
    })
  }, [user])

  useEffect(() => {
    if (appState.user !== null) {
      getUserById(appState.user.uid)
        .then(userData => setAppState(prev => ({ ...prev, userData })))
        .catch(console.log)
    }
  }, [appState.user])

  return (
    <AppContext.Provider value={{ ...appState, setContext: setAppState, addToast }}>
      <Header colorMode={colorMode} toggleColorMode={toggleColorMode}
        setSearchParams={setSearchParams} keyword={keyword} setKeyword={keyword} />
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/login" element={<LogInForm />} />
        <Route path="/register" element={<RegistrationForm />} />
        <Route path="/search" element={<Authenticated user={user}
          loading={loading}><Search keyword={keyword} /></Authenticated>} />
        <Route path="/posts/:id" element={<Authenticated user={user}
          loading={loading}><SinglePost /></Authenticated>} />
        <Route path="/users" element={<Authenticated user={user}
          loading={loading}><Users /></Authenticated>} />
        <Route path="/profile/:username" element={<Authenticated user={user}
          loading={loading}>
          <Profile />
        </Authenticated>} />
        <Route path="/*" element={<NotFound />} />
      </Routes>
    </AppContext.Provider>
  )
}

export default App
