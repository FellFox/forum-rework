import { Flex, Heading, Link, Tag, Text } from "@chakra-ui/react";
import { formatDistance } from "date-fns";
import { useNavigate } from "react-router-dom";

export default function SinglePostContent({ title, content, author, postedOn, tags }) {
  const navigate = useNavigate()

  return (
    <Flex
      direction="column"
      gap={2}
      paddingBlock={3}
      paddingRight={3}
      maxW="100%"
    >
      <Text fontWeight={500} fontSize="sm">
        Posted by <Link onClick={() => navigate(`/profile/${author}`)} fontWeight={600}>u/{author} </Link>
        {postedOn && formatDistance(new Date(postedOn), new Date())} ago
      </Text>
      <Flex gap={3} align="center">
        <Heading fontSize="xl">
          {title}
        </Heading>
        {tags?.map(t => <Tag _hover={{ cursor: "pointer" }} variant="solid" key={t}>{t}</Tag>)}
      </Flex>

      <Text dangerouslySetInnerHTML={{ __html: content }}></Text>
    </Flex>
  )
}
