import { Flex } from "@chakra-ui/react"
import BaseInfoCard from "../../hoc/BaseInfoCard"
import PopularTags from "../PopularTags/PopularTags"

const LeftComponent = () => {
  return (
    <Flex w='25%' direction="column" align="center">
      <BaseInfoCard title="Popular Tags" content={<PopularTags />} />
    </Flex>
  )
}

export default LeftComponent
