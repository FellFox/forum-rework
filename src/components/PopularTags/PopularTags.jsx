import { Flex, Tag } from "@chakra-ui/react";
import { useContext, useEffect, useState } from "react";
import AppContext from "../../context/app.context";
import { getAllTags } from "../../services/tag.services";

export default function PopularTags() {
  const { addToast } = useContext(AppContext)
  const [tags, setTags] = useState([])

  useEffect(() => {
    getAllTags()
      .then(data => setTags(data.sort((a, b) => b.posts.length - a.posts.length)))
      .catch(() => addToast("error", "Something went wrong!"))
  }, [])

  return (
    <Flex gap={2} flexWrap="wrap" justify="center">
      {tags?.map(t => <Tag _hover={{ cursor: "pointer" }} variant="solid" key={t}>{t.tag}</Tag>)}
    </Flex>
  )
}
