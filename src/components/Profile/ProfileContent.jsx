import { Box, Tab, TabList, TabPanel, TabPanels, Tabs, useColorModeValue } from "@chakra-ui/react";
import ProfileComments from "./ProfileComments";
import ProfilePosts from "./ProfilePosts";
export default function ProfileContent({ user }) {
  return (
    <Box
      width="100%"
      boxShadow="0px 5px 10px rgba(0, 0, 0, 0.25)"
      border="1px solid"
      borderRadius={5}
      borderColor={useColorModeValue("light.secondary", "dark.primary")}
      bg={useColorModeValue("light.secondary", "dark.primary")}
    >

      <Tabs>
        <TabList>
          <Tab>Posts</Tab>
          <Tab>Comments</Tab>
        </TabList>
        <TabPanels>
          <TabPanel><ProfilePosts user={user} /></TabPanel>
          <TabPanel><ProfileComments user={user} /></TabPanel>
        </TabPanels>
      </Tabs>
    </Box >

  )
}
