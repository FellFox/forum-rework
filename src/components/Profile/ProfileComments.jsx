import { Box, Flex } from "@chakra-ui/react";
import { useEffect } from "react";
import { useState, useContext } from "react";
import { COMMENTS } from "../../common/enums/sort.enum";
import { getCommentsByAuthor } from "../../services/comments.services";
import Comment from "../Comments/Comment";
import Sort from "../Filters/Sort";
import NoContent from "../../hoc/NoContent";
import AppContext from "../../context/app.context";

export default function ProfileComments({ user }) {
  const [comments, setComments] = useState([]);
  const { userData } = useContext(AppContext)

  useEffect(() => {
    getCommentsByAuthor(user).then(setComments)
  }, [user, userData?.comments])

  return (
    <Box>
      <Flex margin={3}>
        <Sort sortType={COMMENTS} toBeSorted={comments} setSorted={setComments} />
      </Flex>
      <Flex direction="column" p={3} gap={3}>
        {comments?.length === 0 ?
          <NoContent>No comments available for this user</NoContent> :
          comments?.map(c => <Comment key={c.id} comment={c} />)
        }
      </Flex>
    </Box>
  )
}
