import {
  Avatar,
  Box, Flex,
  Heading, SkeletonText,
  Text,
  useColorModeValue
} from "@chakra-ui/react";
import format from "date-fns/format";
import { useEffect, useState } from "react";
import { getAllPosts } from "../../services/posts.services";
import Settings from "../Settings/Settings";

export default function ProfileCard({
  firstName,
  lastName,
  username,
  profilePicture,
  registeredOn,
  posts,
  comments,
  isBlock,
  isOwner = false }) {

  const [allPosts, setAllPosts] = useState()
  const userPosts = allPosts?.filter(post => post.author === username)

  const calculateRating = (rating = 0) => {

    userPosts?.map(post => {
      post.likedBy ? rating += Object.keys(post.likedBy).length : 0
      post.dislikedBy ? rating -= Object.keys(post.dislikedBy).length : 0
    })

    return rating > 0 ? rating : 0
  }

  useEffect(() => {
    getAllPosts().then(setAllPosts).catch(console.error)
  }, [])

  return (
    <Box
      minW={{ base: 280, md: 340 }}
      borderRadius={5}
      border="1px solid"
      borderColor={useColorModeValue("light.font.primary", "dark.font.secondary")}
      boxShadow="0px 3px 6px rgba(0, 0, 0, 0.25)"
      bg={useColorModeValue("light.secondary", "dark.primary")}
      height="max-content"
    >
      <Flex
        padding={5}
        rounded={5}
        gap={4}
        justify="center"
        bg={useColorModeValue("light.primary", "dark.secondary")}
      >
        <Avatar src={profilePicture} size={{ base: "lg", md: "2xl" }} />
        <Box>
          {firstName && lastName ?
            <Heading fontSize="lg">
              {`${firstName} ${lastName}`}
            </Heading>
            : <SkeletonText />}
          <Text fontSize="sm" fontWeight="600" >@{username}</Text>
          {isBlock && <Heading fontSize="sm" variant="lighter">Blocked</Heading>}
          <Box marginTop={2}>
            <Text fontSize="sm">Member since:</Text>
            <Text fontSize="sm" fontWeight={600}>
              {registeredOn && format(registeredOn, "PPP")}
            </Text>
          </Box>
        </Box>
        {isOwner && <Settings />}
      </Flex >
      <Box padding={5}>
        <Flex>
          <Box flexGrow={1}>
            <Heading fontSize="sm">Posts</Heading>
            <Text fontWeight={600} fontSize="sm" >{posts ? Object.keys(posts).length : 0}</Text>
          </Box>
          <Box flexGrow={1}>
            <Heading fontSize="sm">Comments</Heading>
            <Text fontWeight={600} fontSize="sm" >{comments ? Object.keys(comments).length : 0}</Text>
          </Box>
          <Box flexGrow={1}>
            <Heading fontSize="sm">Rating</Heading>
            <Text fontWeight={600} fontSize="sm" >{posts ? calculateRating() : 0}</Text>
          </Box>
        </Flex>
      </Box>
    </Box >
  )
}
