import { Box, Flex } from "@chakra-ui/react";
import { useContext, useEffect, useState } from "react";
import AppContext from "../../context/app.context";
import { POSTS } from "../../common/enums/sort.enum";
import Post from "../../hoc/Post";
import { getPostsByAuthor } from "../../services/posts.services";
import CreatePost from "../CreatePost/CreatePost";
import Filter from "../Filters/Filter";
import Sort from "../Filters/Sort";
import NoContent from "../../hoc/NoContent";
import PostContent from "../Post/PostContent";
export default function ProfilePosts({ user }) {
  const { addToast, userData } = useContext(AppContext)
  const [posts, setPosts] = useState([])
  const [filtered, setFiltered] = useState([])

  useEffect(() => {
    getPostsByAuthor(user).then(setPosts).catch(() => addToast("error", "Something went wrong!"))
  }, [user, userData?.posts])

  useEffect(() => {
    setFiltered([...posts])
  }, [posts])

  return (
    <Box>
      <Flex margin={3} align="center" justify="space-between">
        <Flex gap={2}>
          <Filter unfiltered={posts} setFiltered={setFiltered} />
          <Sort sortType={POSTS} toBeSorted={filtered} setSorted={setFiltered} />
        </Flex>
        {
          !userData?.isBlock ?
            <CreatePost /> :
            <>
            </>
        }
      </Flex>
      {posts.length === 0 ?
        <NoContent>This user has not posted anything yet</NoContent> :
        filtered.length === 0 ?
          <NoContent>No posts matching this criteria</NoContent> :
          filtered.map(p => <Post key={p.id} id={p.id} author={p.author} PostContent={<PostContent {...p} />}
          />)}
    </Box>
  )
}
