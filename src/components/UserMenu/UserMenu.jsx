import { Avatar, Box, Menu, MenuButton, MenuDivider, MenuItem, MenuList, Text } from "@chakra-ui/react";
import { useState } from "react";
import { useEffect } from "react";
import { useContext } from "react";
import { useNavigate } from "react-router-dom";
import AppContext from "../../context/app.context";
import { logoutUser } from "../../services/auth.services";

export default function UserMenu() {
  const { setContext, userData } = useContext(AppContext)
  const [avatar, setAvatar] = useState("")
  const navigate = useNavigate()

  useEffect(() => {
    setAvatar(userData?.profilePicture)
  }, [userData?.profilePicture])
  const handleLogOut = () => {
    logoutUser()
    navigate("/")
    setContext({ user: null, userData: null })
  }

  return (
    <Menu>
      <MenuButton>
        <Avatar src={avatar} />
      </MenuButton>
      <MenuList >
        <MenuItem onClick={() => navigate(`/profile/${userData?.username}`)}>
          <Box>
            <Text>My profile</Text>
            <Text fontWeight={600} fontSize="sm">@{userData?.username}</Text>
          </Box>
        </MenuItem>
        <MenuDivider />
        <MenuItem onClick={handleLogOut}>Log out</MenuItem>
      </MenuList>
    </Menu>
  )
}
