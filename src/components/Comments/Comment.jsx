import { DeleteIcon, EditIcon } from "@chakra-ui/icons"
import { Avatar, Box, Button, Flex, Link, Text, useColorModeValue } from "@chakra-ui/react"
import { formatDistance } from "date-fns"
import { useContext, useEffect, useState } from "react"
import { useNavigate } from "react-router-dom"
import AppContext from "../../context/app.context"
import {
  deleteCommentfromDatabase,
  editCommentInDatabase,
  getCommentById,
  toggleCommentsDisLikes,
  toggleCommentsLikes
} from "../../services/comments.services"
import { getUser } from "../../services/users.services"
import Rating from "../Rating/Rating"
import CreateCommentForm from "./CreateCommentsForm"

export default function Comment({ comment }) {
  const { userData, setContext, addToast } = useContext(AppContext)
  const [authorData, setAuthorData] = useState({})
  const [commentData, setCommentData] = useState({})
  const [currentCommentContent, setCurrentCommentContent] = useState(comment.content)
  const [showEditCommentForm, setShowEditCommentForm] = useState(false)


  useEffect(() => {
    setAuthorData(comment.author)
    setCommentData(comment)
    setCurrentCommentContent(comment.content)
  }, [])
  const navigate = useNavigate()
  const toUserProfile = () => navigate(`/profile/${comment.author}`)

  const upVote = () => {
    if (!userData.likedComments.includes(comment.id)) {
      setContext(state => ({
        ...state,
        userData: {
          ...userData,
          likedComments: [...userData.likedComments, comment.id],
          dislikedComments: userData.dislikedComments.filter(el => el !== comment.id),
        }
      }))

      toggleCommentsLikes(comment.id, userData.username)
      toggleCommentsDisLikes(comment.id, userData.username, false)
    } else {
      setContext(state => ({
        ...state,
        userData: {
          ...userData,
          likedComments: userData.likedComments.filter(el => el !== comment.id)
        }
      }))

      toggleCommentsLikes(comment.id, userData.username, false)
    }
  }

  const downVote = () => {
    if (!userData.dislikedComments.includes(comment.id)) {
      setContext(state => ({
        ...state,
        userData: {
          ...userData,
          dislikedComments: [...userData.dislikedComments, comment.id],
          likedComments: userData.likedComments.filter(el => el !== comment.id)
        }
      }))

      toggleCommentsDisLikes(comment.id, userData.username)
      toggleCommentsLikes(comment.id, userData.username, false)
    } else {
      setContext(state => ({
        ...state,
        userData: {
          ...userData,
          dislikedComments: userData.dislikedComments.filter(el => el !== comment.id),
        }
      }))

      toggleCommentsDisLikes(comment.id, userData.username, false)
    }
  }

  const editComment = async values => {

    if (String(values.commentContent) !== String(currentCommentContent)) {
      try {
        await editCommentInDatabase(comment.id, values.commentContent)
        setCurrentCommentContent(values.commentContent)
      }
      catch (error) {
        addToast("error", "Something went wrong!")
      }

    }
    setShowEditCommentForm(!showEditCommentForm)

  }

  const deleteComment = async () => {
    try {
      await deleteCommentfromDatabase(commentData.id, commentData.postId, commentData.author)
      setCurrentCommentContent(null)
      setContext(state => ({
        ...state,
        userData: {
          ...userData,
          comments: userData.comments.filter(el => el !== comment.id),
        }
      }))
      addToast("success", "You have successfully deleted the comment!")
    }
    catch (error) {
      addToast("error", "Something went wrong!")
    }

  }

  useEffect(() => {
    setCommentData(comment)
    setCurrentCommentContent(comment.content)
    getUser(comment.author)
      .then(data => {
        setAuthorData({ ...data })
      })
      .catch(() => {
        addToast("error", "Something went wrong!")
      })
  }, [comment])

  useEffect(() => {
    if (comment?.id) {
      getCommentById(comment.id)
        .then(setCommentData)
        .catch(() => {
          addToast("error", "Something went wrong!")
        })
    }
  }, [userData?.likedComments, userData?.dislikedComments, comment])

  return (
    <Box
      paddingTop={3}
      paddingInline={4}
      rounded={5}
      border="1px solid"
      boxShadow="0px 2px 2px rgba(0, 0, 0, 0.2)"
      borderColor={useColorModeValue("light.font.primary", "dark.font.secondary")}
      bg={useColorModeValue("light.primary", "dark.primary")}>
      <Flex flexDirection="row">
        <Avatar src={authorData?.profilePicture} size="sm" />
        <Flex flexDirection="column" gap={1}>
          <Text fontSize="sm" fontWeight={500} pl="15px">Commented by
            <Link fontWeight="700" onClick={toUserProfile}> {authorData?.username} </Link>
            {formatDistance(new Date(comment.commentedOn), new Date())}
          </Text>

          {showEditCommentForm ?
            <CreateCommentForm onSubmitFunction={editComment} prevContent={currentCommentContent} />
            :
            <>
              <Text pl="15px" >
                <Box dangerouslySetInnerHTML={{ __html: currentCommentContent }} />
              </Text>
              <Flex flexDirection="row">
                <Rating upVote={upVote} downVote={downVote}
                  rating={(commentData?.likedBy?.length - commentData?.dislikedBy?.length)} direction="row" />
                {(authorData?.username === userData?.username) &&
                  <>
                    {
                      !userData?.isBlock ?
                        <Button variant="ghost" onClick={() => setShowEditCommentForm(!showEditCommentForm)}>
                          <EditIcon />
                        </Button>
                        :
                        <></>
                    }
                    <Button variant="ghost" onClick={() => deleteComment()}>
                      <DeleteIcon />
                    </Button>
                  </>
                }
              </Flex>
            </>
          }
        </Flex>
      </Flex>
    </Box>
  )
}
