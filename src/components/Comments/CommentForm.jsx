
import { useContext } from "react"
import "react-quill/dist/quill.snow.css"
import AppContext from "../../context/app.context"
import { addComment } from "../../services/comments.services"
import CreateCommentForm from "./CreateCommentsForm"

export default function CommentForm({ setComments, postId }) {
  const { userData, setContext, addToast } = useContext(AppContext)
  const publishComment = async value => {
    try {
      const [newCommentId, newComment] =
        await addComment(userData.username, postId, value.commentContent)
      setContext(state => ({
        ...state,
        userData: {
          ...userData,
          comments: [...userData.comments, newCommentId]
        }
      }))

      setComments(coments => [...coments, { ...newComment }])
      addToast("success", "You have successfully added the comment!")
    } catch (e) {
      addToast("error", "Something went wrong!")
    }

  }

  return (
    <>
      {
        !userData?.isBlock ?
          <CreateCommentForm onSubmitFunction={publishComment} /> :
          <>
          </>

      }
    </>
  )
}
