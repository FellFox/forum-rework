import {
  Flex,
  Button,
  Spacer,
  FormErrorMessage,
  FormControl,
  Box,
} from "@chakra-ui/react"
import ReactQuill from "react-quill"
import "react-quill/dist/quill.snow.css"
import { useEffect } from "react"
import { useForm } from "react-hook-form"
import { MAX_COMMENT_LENGTH, MIN_COMMENT_LENGTH } from "../../common/constants/constants"
export default function CreateCommentForm({ onSubmitFunction, prevContent }) {
  const {
    handleSubmit,
    register,
    watch,
    setValue,
    formState: {
      errors,
      isSubmitting
    }
  } = useForm()

  const onSubmit = async values => {
    await onSubmitFunction(values)
    if (prevContent === undefined) {
      setValue("commentContent", "")
    }
  }

  const editorContent = watch("commentContent")
  const onEditorStateChange = editorState => {
    setValue("commentContent", editorState)
  }

  useEffect(() => {
    if (prevContent && prevContent !== "") {
      setValue("commentContent", prevContent)
    }
  }, [])
  useEffect(() => {
    register("commentContent", {
      required: "Please enter a value!",
      minLength: {
        value: MIN_COMMENT_LENGTH,
        message: `Minimum length should be ${MIN_COMMENT_LENGTH}`
      },
      maxLength: {
        value: MAX_COMMENT_LENGTH,
        message: `Maximum length should be ${MAX_COMMENT_LENGTH}`
      },
      validate:
      {

        emptyComment: value => {
          return (value.replace(/<(.|\n)*?>/g, "").trim().length !== 0)
            || "Please enter a value!"
        }
      }
    })
  }, [register])


  return (
    <Box>
      <FormControl id="commentContent" isRequired
        isInvalid={
          errors.commentContent
        }>
        <ReactQuill
          theme="snow"
          value={editorContent}
          onChange={onEditorStateChange}
          id='commentContentTextEditor' />
        <Flex marginInline={3} align="center">
          <FormErrorMessage> {
            errors.commentContent && errors.commentContent.message
          } </FormErrorMessage>
          <Spacer />
          <Button marginTop={3}
            onClick={handleSubmit(onSubmit)}
            isLoading={isSubmitting}
            variant="accent"
            size="sm">
            Comment
          </Button>
        </Flex>
      </FormControl>
    </Box>
  )
}
