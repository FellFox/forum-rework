import { Flex } from "@chakra-ui/react";
import Comment from "./Comment";

export default function Comments({ comments }) {
  return (
    <Flex direction="column" my={5}>
      <Flex flexDirection="column" gap={3}>
        {comments.map((c, index) => <Comment key={index} comment={c} />)}
      </Flex >
    </Flex >
  )
}
