import { ChevronDownIcon } from "@chakra-ui/icons";
import {
  Button,
  Flex, Menu, MenuButton, MenuItemOption, MenuList, MenuOptionGroup, Text
} from "@chakra-ui/react";
import { GENRES, POST_TYPE } from "../../common/enums/tags.enum";

export default function Tags({ tags, setTags }) {
  return (
    <Flex justify="start" align="center" gap={5}>
      <Text>Tags</Text>
      <Menu>
        <MenuButton variant="outlined" rounded={20} size="sm" as={Button} rightIcon={<ChevronDownIcon />}>
          {tags.postType ? tags.postType : "Post type"}
        </MenuButton>
        <MenuList minWidth='240px'>
          <MenuOptionGroup title='Post type' type='radio'>
            {Object.values(POST_TYPE)
              .map(type => <MenuItemOption key={type} value={type}
                onClick={() => setTags(({ ...tags, postType: type }))}>{type}</MenuItemOption>)}
          </MenuOptionGroup>
        </MenuList>
      </Menu>
      <Menu>
        <MenuButton variant="outlined" rounded={20} size="sm" as={Button} rightIcon={<ChevronDownIcon />}>
          {tags.genre ? tags.genre : "Book genre"}
        </MenuButton>
        <MenuList minWidth='240px'>
          <MenuOptionGroup title='Book genre' type='radio'>
            {Object.values(GENRES)
              .map(genre => <MenuItemOption key={genre} value={genre}
                onClick={() => setTags(({ ...tags, genre: genre }))}>{genre}</MenuItemOption>)}
          </MenuOptionGroup>
        </MenuList>
      </Menu>
    </Flex>
  )
}
