import { SearchIcon, SunIcon, MoonIcon } from "@chakra-ui/icons"
import {
  Box,
  Flex,
  Button,
  ButtonGroup,
  Input,
  InputGroup,
  InputRightElement
} from "@chakra-ui/react"
import { useContext } from "react"
import { Link } from "react-router-dom"
import { useNavigate } from "react-router-dom"
import AppContext from "../../context/app.context"
import Logo from "../Logo/Logo"
import UserMenu from "../UserMenu/UserMenu"


function Header({ colorMode, toggleColorMode, setSearchParams, keyword }) {
  const navigate = useNavigate()
  const { user } = useContext(AppContext)

  const handleChange = e => {
    setSearchParams(e.target.value)
  }
  const handleSubmit = e => {
    navigate(`/search/?query=${keyword}`)
    e.preventDefault()

  }

  return (
    <>
      <Box
        bg={colorMode === "light" ? "light.secondary" : "dark.primary"}
        boxShadow="0px 5px 5px rgba(0, 0, 0, 0.25)"
      >
        <Flex justify="space-around" align="center" gap="2rem">
          <Link to="/">
            <Logo mode={colorMode} />
          </Link>
          <InputGroup maxW={500}>
            <InputRightElement children={
              <Button
                keyword={keyword}
                boxShadow="none"
                borderRadius={5}
                onClick={handleSubmit}
              >
                <SearchIcon />
              </Button>
            } />
            <Input
              variant="filled"
              placeholder="Search"
              value={keyword}
              onChange={handleChange}
              size='md'
            />
          </InputGroup>
          <ButtonGroup gap={2} alignItems="center">
            <Button
              size="sm"
              aria-label="Toggle Color Mode"
              onClick={toggleColorMode}
              w="fit-content"
            >
              {colorMode === "light" ? <MoonIcon /> : <SunIcon />}
            </Button>
            {user === null ?
              <ButtonGroup>
                <Button paddingInline={7} onClick={() => navigate("/login")}>Log in</Button>
                <Button paddingInline={7} variant="accent" onClick={() => navigate("/register")}>
                  Sign up
                </Button>
              </ButtonGroup>
              : <UserMenu />}
          </ButtonGroup>
        </Flex>
      </Box>
    </>
  )
}

export default Header
