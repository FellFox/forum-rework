import { Button, Flex } from "@chakra-ui/react";
import { useDisclosure } from "@chakra-ui/react"
import ModalForm from "../Modals/ModalForm";
import CreateNewPost from "../../views/Forms/CreatePostForm"
import { AddIcon } from "@chakra-ui/icons";
import AppContext from "../../context/app.context";
import { useContext, useState } from "react";
import { addPost } from "../../services/posts.services";
import { addTagToPost } from "../../services/tag.services";

export default function CreatePost() {
  const { setContext, userData,addToast } = useContext(AppContext)

  const [tags, setTags] = useState({
    postType: "",
    genre: "",
  });

  const { isOpen, onOpen, onClose } = useDisclosure()

  const addTagsToPost = postId => {
    const validTags = Object.values(tags).filter(t => t !== "");

    if (validTags.length === 0) return

    try {
      validTags.map(tag => addTagToPost(tag, postId))
      setTags({ postType: "", genre: "" })
    } catch (e) {
      addToast("error","Something went wrong!")
    }
  }

  const publishPost = async values => {
    try {
      const postId = await addPost(values.title, values.content, userData?.username)
      addTagsToPost(postId);

      setContext(state => ({
        ...state,
        userData: {
          ...userData,
          posts: [...userData.posts, postId]
        }
      }))
      addToast("success","You have successfully added a post!")
      onClose()
    }
    catch (error) {
      addToast("error","Something went wrong!")
    }
  }

  return (
    <div>
      <Button onClick={onOpen} variant="accent" size={{ base: "md" }}>
        <Flex gap={2} align="center">
          New Post
          <AddIcon />
        </Flex>
      </Button>
      <ModalForm isOpen={isOpen} onClose={onClose} >
        <CreateNewPost tags={tags} setTags={setTags} onSubmitFunction={publishPost} prevContent="" />
      </ModalForm>
    </div>
  )
}
