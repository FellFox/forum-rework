import { Table, TableContainer, Tbody, Th, Thead, Tr, useColorModeValue, Flex } from "@chakra-ui/react";
import { useContext } from "react";
import { useEffect, useState } from "react";
import AppContext from "../../context/app.context";
import { getAllUsers } from "../../services/users.services";
import { getAllPosts } from "../../services/posts.services"

import TopUser from "./TopUser";
export default function TopUsers() {
  const [users, setUsers] = useState()
  const [posts, setPosts] = useState()
  const [sortedUsers, setSortedUsers] = useState([])
  const { userData } = useContext(AppContext)

  const calculateRating = user => {

    const userPosts = posts?.filter(post => post.author === user.username)

    let rating = 0
    userPosts?.map(post => {
      post.likedBy ? rating += Object.keys(post.likedBy).length : 0
      post.dislikedBy ? rating -= Object.keys(post.dislikedBy).length : 0
    })
    user.rating = rating

    return user.rating > 0 ? rating : 0
  }

  useEffect(() => {
    getAllPosts().then(setPosts).catch(console.error)
  }, [userData])

  useEffect(() => {
    getAllUsers().then(setUsers).catch(console.error)
  }, [userData])

  useEffect(() => {
    getAllUsers().then(data => {
      setSortedUsers(data.sort((a, b) => calculateRating(b) - calculateRating(a)).slice(0, 10))
    }).catch(console.error)
  }, [users])

  return (
    <TableContainer
      bg={useColorModeValue("light.secondary", "dark.primary")}
      boxShadow="0px 2px 3px rgba(0, 0, 0, 0.25)"
    >
      <Table size="sm">
        <Thead>
          <Tr>
            <Th><Flex justifyContent='center'>№</Flex></Th>
            <Th><Flex justifyContent='center'>User</Flex></Th>
            <Th><Flex justifyContent='center'>rating</Flex></Th>
          </Tr>
        </Thead>
        <Tbody>
          {sortedUsers.map((u, i) => <TopUser key={u.uid} pos={i + 1} user={u} />)}
        </Tbody>
      </Table>
    </TableContainer>
  )
}
