import { Avatar, Flex, Link } from "@chakra-ui/react";

import { Td, Tr, useColorModeValue } from "@chakra-ui/react";
import { useNavigate } from "react-router-dom";
export default function TopUser({ user, pos }) {
  const navigate = useNavigate()
  
  return (
    <Tr  bg={useColorModeValue("light.primary", "dark.secondary")}>
      <Td p="5px" ><Flex justifyContent='center'>{pos}</Flex></Td>
      <Td p="5px" >
        <Flex align="center" gap={2}>
          <Avatar src={user.profilePicture} size="sm" />
          <Link onClick={() => navigate(`/profile/${user.username}`)}>{user.username}</Link>
        </Flex>
      </Td>
      <Td p="5px"><Flex justifyContent='center'>{user.rating}</Flex></Td>
    </Tr >
  )
}
