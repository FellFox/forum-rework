import { ChevronDownIcon } from "@chakra-ui/icons";
import { Box, Button, Menu, MenuButton, MenuDivider, MenuItem, MenuList } from "@chakra-ui/react";
import { ASC, DESC, POSTS, USERS } from "../../common/enums/sort.enum";

export default function Sort({ sortType, toBeSorted, setSorted }) {
  const sortBy = (criteria, order) => {
    if (criteria === sortType.BY_DATE) {
      setSorted([...toBeSorted.sort((a, b) =>
        order === ASC ? (b[criteria] - a[criteria]) : (a[criteria] - b[criteria]))])
    } else if (criteria === sortType.BY_USERNAME) {
      setSorted([...toBeSorted.sort((a, b) =>
        order === ASC ? a[criteria].localeCompare(b[criteria]) : b[criteria].localeCompare(a[criteria])
      )])
    } else {
      setSorted([...toBeSorted.sort((a, b) =>
        order === ASC ?
          (b[criteria] ? Object.keys(b[criteria]).length : 0)
          - (a[criteria] ? Object.keys(a[criteria]).length : 0) :
          (a[criteria] ? Object.keys(a[criteria]).length : 0)
          - (b[criteria] ? Object.keys(b[criteria]).length : 0))])
    }
  }

  return (
    <Box>
      <Menu>
        <MenuButton size="sm" as={Button} rightIcon={<ChevronDownIcon />}>
          Sort by
        </MenuButton>
        <MenuList>
          {sortType === USERS && <>
            <MenuItem onClick={() => sortBy(sortType.BY_USERNAME, ASC)}>
              Username (A-z)
            </MenuItem>
            <MenuItem onClick={() => sortBy(sortType.BY_USERNAME, DESC)}>
              Username (Z-a)
            </MenuItem><MenuDivider /></>}
          <MenuItem onClick={() => sortBy(sortType.BY_DATE, ASC)}>
            Newest
          </MenuItem>
          <MenuItem onClick={() => sortBy(sortType.BY_DATE, DESC)}>
            Oldest
          </MenuItem>
          {sortType !== USERS && <><MenuDivider />
            <MenuItem onClick={() => sortBy(sortType.BY_LIKES, ASC)}>
              Most liked
            </MenuItem>
            <MenuItem onClick={() => sortBy(sortType.BY_LIKES, DESC)}>
              Least liked
            </MenuItem></>}
          {sortType === POSTS &&
            <>
              <MenuDivider />
              <MenuItem onClick={() => sortBy(POSTS.BY_COMMENTS, ASC)}>
                Most commented
              </MenuItem>
              <MenuItem onClick={() => sortBy(POSTS.BY_COMMENTS, DESC)}>
                Least commented
              </MenuItem>
            </>}
        </MenuList>
      </Menu>
    </Box>
  )
} 
