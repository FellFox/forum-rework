import { ChevronDownIcon } from "@chakra-ui/icons";
import { Box, Button, Menu, MenuButton, MenuItemOption, MenuList, MenuOptionGroup } from "@chakra-ui/react";
import { useEffect, useState } from "react";
import { GENRES, POST_TYPE } from "../../common/enums/tags.enum";

export default function Filter({ unfiltered, setFiltered }) {
  const [filters, setFilters] = useState({
    postType: "",
    genre: "",
  })

  const filterBy = () => {
    setFiltered(unfiltered.filter(item =>
      (filters.postType ? item.tags.includes(filters.postType) : item)
      && (filters.genre ? item.tags.includes(filters.genre) : item)
    ))
  }

  useEffect(() => {
    filterBy()
  }, [filters])

  return (
    <Box>
      <Menu closeOnSelect={false}>
        <MenuButton size="sm" as={Button} rightIcon={<ChevronDownIcon />}>
          Filter by
        </MenuButton>
        <MenuList h={200} css={{
          overflowY: "scroll",
          "&::-webkit-scrollbar": {
            width: "4px",
          },
          "&::-webkit-scrollbar-track": {
            width: "6px",
          },
          "&::-webkit-scrollbar-thumb": {
            background: "lightgrey",
            borderRadius: "24px",
          },
        }}
        >
          <MenuOptionGroup title="Post type" type="radio">
            <MenuItemOption value="Any" onClick={() => setFilters({ ...filters, postType: "" })}>Any</MenuItemOption>
            {Object.values(POST_TYPE).map(postType => <MenuItemOption key={postType} value={postType}
              onClick={() => setFilters({ ...filters, postType })}>{postType}</MenuItemOption>)}
          </MenuOptionGroup>
          <MenuOptionGroup title="Book genre" type="radio">
            <MenuItemOption value="Any" onClick={() => setFilters({ ...filters, genre: "" })}>Any</MenuItemOption>
            {Object.values(GENRES).map(genre => <MenuItemOption key={genre} value={genre}
              onClick={() => setFilters({ ...filters, genre })}>{genre}</MenuItemOption>)}
          </MenuOptionGroup>
        </MenuList>
      </Menu>
    </Box>
  )
} 
