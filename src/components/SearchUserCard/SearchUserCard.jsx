import { Avatar, Box, Flex, Heading, SkeletonText, Text, useColorModeValue } from "@chakra-ui/react"
import { format } from "date-fns"
import { useNavigate } from "react-router-dom"

export default function SearchUserCard({
  username,
  firstName,
  lastName,
  profilePicture,
  registeredOn
}) {
  const navigate = useNavigate()
  const toUserProfile = () => navigate(`/profile/${username}`)

  return (
    <Box
      borderRadius={5}
      boxShadow="0px 3px 6px rgba(0, 0, 0, 0.25)"
      bg={useColorModeValue("light.secondary", "dark.primary")}
      height="max-content"
      onClick={toUserProfile}
    >
      <Flex
        padding={5}
        rounded={5}
        gap={4}
        justify="space-evenly"
        bg={useColorModeValue("light.primary", "dark.secondary")}
      >
        <Avatar src={profilePicture} size={{ base: "lg" }} />
        <Box>
          {firstName && lastName ?
            <Heading fontSize="md">
              {`${firstName} ${lastName}`}
            </Heading>
            : <SkeletonText />}
          <Text fontSize="sm" fontWeight="600" >@{username}</Text>
          <Box marginTop={2}>
            <Text fontSize="sm">Member since:</Text>
            <Text fontSize="sm" fontWeight={600}>
              {registeredOn && format(registeredOn, "PPP")}
            </Text>
          </Box>
        </Box>
      </Flex >
    </Box>
  )
}
