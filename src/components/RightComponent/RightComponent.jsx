import { Flex } from "@chakra-ui/react"
import Stats from "../Stats/Stats"
import BaseInfoCard from "../../hoc/BaseInfoCard"
import TopUsers from "../TopUsers/TopUsers"

const RightComponent = () => {

  return (
    <Flex w='25%' direction="column" align="center" gap={10}>
      <BaseInfoCard content={<Stats userCount={19} postCount={58} />} />
      <BaseInfoCard title="Top Users" content={<TopUsers />} />
    </Flex>
  )
}

export default RightComponent
