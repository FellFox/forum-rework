import { Box, Divider, Flex, Link, Stat, StatLabel, StatNumber, useColorModeValue } from "@chakra-ui/react"
import { useContext, useEffect } from "react"
import { useState } from "react"
import { useNavigate } from "react-router-dom"
import { getAllComments } from "../../services/comments.services"
import { getAllPosts } from "../../services/posts.services"
import { getAllUsers } from "../../services/users.services"
import AppContext from "../../context/app.context"

export default function Stats() {

  const { userData } = useContext(AppContext)
  const [usersCount, setUsersCount] = useState(0)
  const [postsCount, setPostsCount] = useState(0)
  const [commentsCount, setCommentsCount] = useState(0)

  const navigate = useNavigate()
  const showAllUsers = () => {
    navigate("/users")
  }
  const showAllPosts = () => {
    navigate("/")
  }

  useEffect(() => {
    getAllUsers().then(users => setUsersCount(users.length)).catch(console.error)
  }, [usersCount])

  useEffect(() => {
    getAllPosts().then(posts => setPostsCount(posts.length)).catch(console.error)
  }, [userData?.posts])

  useEffect(() => {
    getAllComments().then(comments => setCommentsCount(comments.length)).catch(console.error)
  }, [userData?.comments])

  return (
    <Box borderColor={useColorModeValue("light.font.secondary", "dark.font.secondary")}>
      <Flex>
        <Stat>
          <StatLabel onClick={showAllUsers}><Link>Users</Link></StatLabel>
          <StatNumber>{usersCount}</StatNumber>
        </Stat>
        <Stat>
          <StatLabel onClick={showAllPosts}><Link>Posts</Link></StatLabel>
          <StatNumber>{postsCount}</StatNumber>
        </Stat>
        <Stat>
          <StatLabel>Comments</StatLabel>
          <StatNumber>{commentsCount}</StatNumber>
        </Stat>
      </Flex>
      <Divider marginTop={2} />
    </Box>
  )
}
