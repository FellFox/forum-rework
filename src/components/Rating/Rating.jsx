import { ArrowDownIcon, ArrowUpIcon } from "@chakra-ui/icons";
import { Button, Flex, Text } from "@chakra-ui/react";
import { useContext } from "react";
import { useNavigate } from "react-router-dom";
import AppContext from "../../context/app.context";

export default function Rating({ upVote, downVote, rating, direction = "column" }) {
  const { ...appState } = useContext(AppContext)
  const navigate = useNavigate()

  const navigateToLogin = () => {
    navigate("/login")
  }

  return (
    <Flex direction={direction} align="center">
      <Button variant="ghost" onClick={appState.user ? upVote : navigateToLogin}>
        <ArrowUpIcon />
      </Button>
      <Text fontWeight={600} fontSize="md">
        {rating}
      </Text>
      <Button variant="ghost" onClick={appState.user ? downVote : navigateToLogin}>
        <ArrowDownIcon />
      </Button>
    </Flex>
  )
}
