import LightLogo from "../../assets/logo/light.svg"
import DarkLogo from "../../assets/logo/dark.svg"
import { Flex, Heading, Image } from "@chakra-ui/react"

export default function Logo({ mode }) {
  return (
    <Flex align="center" gap={1}>
      <Heading size="lg">BOOK</Heading>
      <Image boxSize="70" src={mode === "light" ? LightLogo : DarkLogo} />
      <Heading size="lg">ROOM</Heading>
    </Flex>
  )
}
