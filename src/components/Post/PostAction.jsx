import { DeleteIcon, EditIcon } from "@chakra-ui/icons"
import { Box, Button, Center, Flex, useDisclosure } from "@chakra-ui/react"
import { useContext, useState } from "react"
import AppContext from "../../context/app.context"
import { deleteCommentForPost, getCommentForPost } from "../../services/comments.services"
import { deletePostfromDatabase, editPostInDatabase, getPostById } from "../../services/posts.services"
import CreateNewPost from "../../views/Forms/CreatePostForm"
import ModalForm from "../Modals/ModalForm"
import { useNavigate, useLocation } from "react-router-dom"


export default function PostAction({ postId }) {
  const navigate = useNavigate()
  const location = useLocation();
  const { setContext, userData, addToast } = useContext(AppContext)
  const { isOpen, onOpen, onClose } = useDisclosure()
  const confirmationModal = useDisclosure()
  const [postInfo, setPostInfo] = useState({})


  const showEditForm = async () => {
    try {
      const info = await getPostById(postId)
      setPostInfo(info)
      onOpen()
    } catch (e) {
      addToast("error", "Something went wrong")
    }
  }

  const deletePost = async () => {
    getCommentForPost(postId)
      .then(commentsFromDB => {
        const ids = Object.keys(commentsFromDB)
        return Promise.all(ids.map(comment => deleteCommentForPost(postId, comment)))
      })
      .catch(() => {
        addToast("error", "Something went wrong!")
      })


    await deletePostfromDatabase(postId, userData.username).
      catch(() => {
        addToast("error", "Something went wrong!")
      })
    setContext(state => ({
      ...state,
      userData: {
        ...userData,
        posts: [...userData.posts].filter(el => el !== postId)
      }
    }))

    addToast("success", "You have successfully deleted the post!")
    if (location.pathname.includes("posts")) {
      navigate("/")
    }
  }

  const editPost = async values => {
    const newPostValues = { ...postInfo }
    if (values.title !== postInfo.title) {
      editPostInDatabase(postId, "title", values.title).
        catch(() => addToast("success", "You have successfully updated the post!"))
      newPostValues.title = values.title
    }
    if (values.content !== postInfo.content) {
      editPostInDatabase(postId, "content", values.content)
      newPostValues.content = values.content
    }
    setPostInfo(newPostValues)
    setContext(state => ({
      ...state,
      userData: {
        ...userData,
        posts: [...userData.posts]
      }
    }))
    addToast("success", "You have successfully updated the post!")
    onClose()
  }
  return (
    <Box>
      <Flex direction="row" align="center">
        {
          !userData?.isBlock ?
            <Button variant="ghost">
              <EditIcon onClick={showEditForm} />
            </Button>
            :
            <></>
        }

        <Button variant="ghost">
          <DeleteIcon onClick={confirmationModal.onOpen} />
        </Button>
        <ModalForm isOpen={confirmationModal.isOpen} onClose={confirmationModal.onClose} >
          <Center>Are you sure you want to delete the post?  </Center>
          <Flex justify="center" align="center" gap={4} my={5}>
            <Button size={{ base: "sm" }} onClick={confirmationModal.onClose} >Cancel</Button>
            <Button variant="accent" size={{ base: "sm" }} mr={3} onClick={() => deletePost()} >Delete</Button>
          </Flex>
        </ModalForm>
        <ModalForm isOpen={isOpen} onClose={onClose} >
          <CreateNewPost onSubmitFunction={editPost} prevContent={postInfo} />
        </ModalForm>
      </Flex>
    </Box>
  )
}
