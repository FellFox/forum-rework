import { Flex, Heading, Tag, Text, Box, Link as ChakraLink } from "@chakra-ui/react";
import { useNavigate } from "react-router-dom";
import { formatDistance } from "date-fns";

export default function PostContent({ id, title, content, author, postedOn, tags }) {
  const navigate = useNavigate()
  const showPostDetails = () => {
    navigate(`/posts/${id}`)
  }

  return (
    <Flex
      direction="column"
      gap={2}
      paddingBlock={3}
      maxH="300px"
      maxW="100%"
    >
      <Text fontWeight={500} fontSize="sm"> Posted by
        <ChakraLink onClick={() => navigate(`/profile/${author}`)} fontWeight={600}> u/{author} </ChakraLink>
        {postedOn && formatDistance(new Date(postedOn), new Date())} ago
      </Text>
      <Flex gap={3} align="center">
        <ChakraLink><Heading fontSize="xl" onClick={showPostDetails}>
          <Box dangerouslySetInnerHTML={{ __html: title }} />
        </Heading></ChakraLink>
        {tags?.map(t => <Tag _hover={{ cursor: "pointer" }} variant="solid" key={t}>{t}</Tag>)}
      </Flex>
      <Text noOfLines={3}>
        <Box dangerouslySetInnerHTML={{ __html: content }} />
      </Text>
    </Flex >
  )
}
