import { Button, Avatar, Link, Td, Tr, useColorModeValue, Text, Flex } from "@chakra-ui/react";
import { format } from "date-fns";
import { useContext } from "react";
import { useNavigate } from "react-router-dom";
import { userRole } from "../../common/enums/user-role.enum";
import AppContext from "../../context/app.context";
import { updateUserData } from "../../services/users.services";
export default function SingleUser({ profilePicture, username, isBlock = false, rating, posts = 0, registeredOn, setBlocked }) {
  const { userData, addToast } = useContext(AppContext)

  const navigate = useNavigate()
  const toUserProfile = () => navigate(`/profile/${username}`)

  const blockUser = async (user, currentStatus) => {
    await updateUserData("isBlock", !currentStatus, user).catch(() => {
      addToast("error", "Something went wrong!")
    })

    setBlocked(blocked => [...blocked, user])
  }

  return (
    <Tr
      bg={useColorModeValue("light.primary", "dark.secondary")}>
      <Td><Avatar onClick={toUserProfile} src={profilePicture} size="md" /></Td>
      <Td><Link onClick={toUserProfile}>{username}</Link></Td>
      <Td>{rating}</Td>
      <Td>{Object.keys(posts).length}</Td>
      <Td>
        <Flex align="center" justify="space-between">
          <Text>{format(registeredOn, "PPP")}</Text>
          {(userData && userData.role === userRole.ADMIN) &&
            <Button size="xs" onClick={() => blockUser(username, isBlock)}>{isBlock ? "Unblock" : "Block"}</Button>}
        </Flex>
      </Td>
    </Tr >
  )
}
