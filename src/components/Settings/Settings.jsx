import { SettingsIcon } from "@chakra-ui/icons"
import { Button, useDisclosure } from "@chakra-ui/react"
import SettingsForm from "../../views/Forms/SettingsForm"
import ModalForm from "../Modals/ModalForm"

export default function Settings() {
  const { isOpen, onOpen, onClose } = useDisclosure()

  return (
    <div>
      <Button onClick={onOpen} variant="ghost" size="sm" >
        <SettingsIcon />
      </Button>
      <ModalForm header="Account settings" isOpen={isOpen} onClose={onClose}>
        <SettingsForm onClose={onClose}/>
      </ModalForm>
    </div>
  )
}
