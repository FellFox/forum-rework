import { Box, Flex } from "@chakra-ui/react";
import Sort from "../Filters/Sort";
import NoContent from "../../hoc/NoContent";
import Post from "../../hoc/Post";
import SearchPostContent from "../SinglePostContent/SearchPostContent";
import { POSTS } from "../../common/enums/sort.enum";
import { useState, useEffect } from "react";
import { getAllPosts } from "../../services/posts.services";
import Filter from "../Filters/Filter";

export default function SearchResultsPosts({ keyword }) {
  const [posts, setPosts] = useState([])
  const [filtered, setFiltered] = useState([])

  useEffect(() => {
    getAllPosts().then(data =>
      setPosts(data?.filter(post =>

        post?.title.toLocaleLowerCase().includes(keyword.toLocaleLowerCase())
        ||
        post?.content.toLocaleLowerCase().includes(keyword.toLocaleLowerCase())
        ||
        post?.author.toLocaleLowerCase().includes(keyword.toLocaleLowerCase())
        ||
        post?.tags?.some(tag => tag.toLocaleLowerCase().includes(keyword.toLocaleLowerCase()))
      )))
      .catch(console.error)
  }, [keyword])

  useEffect(() => {
    setFiltered([...posts])
  }, [posts])

  return (
    <Box>
      <Flex margin={4} align="center" gap={2}>
        <Filter unfiltered={posts} setFiltered={setFiltered} />
        <Sort sortType={POSTS} toBeSorted={filtered} setSorted={setFiltered} />
      </Flex>
      {posts.length === 0 ?
        <NoContent>No posts found</NoContent> :
        filtered.length === 0 ?
          <NoContent>No posts matching this criteria</NoContent> :
          filtered.map((p, i) => <Post id={p.id} key={i} PostContent={<SearchPostContent {...p} />} />)}
    </Box>
  )
}
