import { Box, Flex, Grid, GridItem } from "@chakra-ui/react";
import { useEffect, useState } from "react";
import { USERS } from "../../common/enums/sort.enum";
import { getAllUsers } from "../../services/users.services";
import Sort from "../Filters/Sort";
import NoContent from "../../hoc/NoContent";
import SearchUserCard from "../SearchUserCard/SearchUserCard";

export default function SearchResultsUsers({ keyword }) {
  const [users, setUsers] = useState([])

  useEffect(() => {
    getAllUsers().then(data =>
      setUsers(data?.filter(user =>
        user.username.toLocaleLowerCase().includes(keyword.toLocaleLowerCase())
        ||
        user.firstName.toLocaleLowerCase().includes(keyword.toLocaleLowerCase())
        ||
        user.lastName.toLocaleLowerCase().includes(keyword.toLocaleLowerCase())
      ))).catch(console.error)
  }, [keyword])

  return (
    <Box>
      <Flex margin={4} align="center" justify="space-between">
        <Sort sortType={USERS} toBeSorted={users} setSorted={setUsers} />
      </Flex>
      <Grid margin={4} gap={4} templateColumns="repeat(3, 1fr)">
        {users.length === 0 ?
          <GridItem colSpan={3}><NoContent>No users found</NoContent></GridItem> :
          users.map((u, i) => <GridItem key={i}><SearchUserCard {...u} /></GridItem>)}
      </Grid>
    </Box >
  )
}
