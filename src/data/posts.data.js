export const posts = [
  {
    id: "1",
    title: "MyTitle",
    description: `Lorem ipsum dolor sit amet consectetur adipisicing elit. Ullam, voluptatem iste. 
    Beatae eveniet non maiores porro quasi neque animi quis dolores, delectus hic illum repellendus 
    magni cumque sit recusandae sunt minima! Dolorem eveniet illo temporibus soluta eaque ea blanditiis, 
    voluptatum dolor consequuntur omnis velit veritatis, reprehenderit reiciendis deserunt consectetur 
    ducimus molestiae architecto natus provident. Voluptas, quos. Quidem eligendi quia labore temporibus`,
    tags: ["hello", "hello2"],
    comments: [`<p><strong>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ullam, voluptatem iste. 
    Beatae eveniet non maiores porro quasi neque animi quis dolores, delectus hic illum repellendus`,
    `recusandae sunt minima! Dolorem eveniet illo temporibus soluta eaque ea blanditiis, 
    voluptatum dolor consequuntur omnis velit veritatis, reprehenderit reiciendis deserunt consectetur 
    ducimus molestiae architecto natus provident<strong/></p>`]
  },

  {
    id: "2",
    title: "MyTitle2",
    description: "MyDescription2",
    tags: ["#not so general"],
    comments: ["<p><strong>dasd<strong/></p>"]
  },

  {
    id: "3",
    title: "MyTitle3 sfasasfasfasfasfasfasfasfasfasfasfasf",
    description: "MyDescription3 asdasdasgdfsgdlgoijhans[pvmasd[jv[asdfvm[asfdjvb[oafbv[oadnbf",
    tags: ["#general"],
    comments: ["<p><strong style=\"color: rgb(230, 0, 0);\">asdasd</strong></p>"]
  }
];


export const usersList = [
  {
    id:1,
    name: "pesho",
    score: 99,
    posts: 40,
    joined: "01.03.2020"
  },
  {
    id:2,
    name: "gosho",
    score: 90,
    posts: 15,
    joined: "02.05.2021"
  },
  {
    id:3,
    name: "tosho",
    score: 100,
    posts: 5,
    joined: "07.11.2019"
  }
]
