import { ADMIN_ROLE, USER_ROLE } from "../constants/constants";

export const users = {
  kokiqn: {
    uid: "user_id1",
    role: ADMIN_ROLE,
    firstName: "Kaloyan",
    lastName: "Spasov",
    email: "kaloyan.spasov.a42@learn.telerikacademy.com",
    registeredOn: new Date("15/08/2022 12:48:23"),
    profilePicture: "../assets/images/mockavatar.jpg",
    posts: {
      "post_id1": true,
      "post_id4": true,
    },
    comments: {
      "comment_id3": true,
      "comment_id9": true,
    },
    likedPosts: {
      "post_id2": true,
    },
    dislikedPosts: {
      "post_id9": true,
    },
    likedComments: {
      "comment_id7": true,
    },
    dislikedCommnets: {
      "comment_id10": true,
    }
  },
  Rumi: {
    uid: "user_id2",
    role: ADMIN_ROLE,
    firstName: "Rumyana",
    lastName: "Sirakova",
    email: "rumyana.sirakova.a42@learn.telerikacademy.com",
    registeredOn: new Date("15/08/2022 12:48:23"),
    profilePicture: "",
    posts: {
      "post_id3": true,
      "post_id6": true,
    },
    comments: {
      "comment_id2": true,
      "comment_id3": true,
    },
    likedPosts: {
      "post_id2": true,
    },
    dislikedPosts: {
      "post_id9": true,
    },
    likedComments: {
      "comment_id7": true,
    },
    dislikedCommnets: {
      "comment_id10": true,
    }
  },
  pesho: {
    uid: "user_id3",
    role: USER_ROLE,
    firstName: "Pesho",
    lastName: "Petrov",
    email: "pesho@abv.bg",
    registeredOn: new Date("15/08/2022 12:48:23"),
    profilePicture: "",
    posts: {
      "post_id7": true,
      "post_id8": true,
    },
    comments: {
      "comment_id5": true,
      "comment_id6": true,
    },
    likedPosts: {
      "post_id2": true,
    },
    dislikedPosts: {
      "post_id9": true,
    },
    likedComments: {
      "comment_id7": true,
    },
    dislikedCommnets: {
      "comment_id10": true,
    }
  }
}
