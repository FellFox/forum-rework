import { initializeApp } from "firebase/app"
import { getAuth } from "firebase/auth"
import { getDatabase } from "firebase/database"
import { getStorage } from "firebase/storage";



const firebaseConfig = {
  apiKey: "AIzaSyCgs0IEZ8lyavwB0fS10Atv1U5Tlxi-yAQ",
  authDomain: "forum-system-4379a.firebaseapp.com",
  projectId: "forum-system-4379a",
  storageBucket: "forum-system-4379a.appspot.com",
  messagingSenderId: "898552020203",
  appId: "1:898552020203:web:b6fc62458b6094f46d92ae",
  measurementId: "G-R2W739XY11",
  databaseURL: "https://forum-system-4379a-default-rtdb.europe-west1.firebasedatabase.app/",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
// the Firebase authentication handler
export const auth = getAuth(app);
// the Realtime Database handler
export const db = getDatabase(app);

export const storage = getStorage(app);